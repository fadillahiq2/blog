<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class BlogController extends Controller
{
    public function index()
    {
        Paginator::useBootstrap();
        $categories = Category::with(['post.user'])->get();
        $posts = Post::with(['category', 'tags', 'user'])->latest()->paginate(10);
        $navs = Category::with(['post.user'])->where('name', 'PC')->orWhere('name', 'Console')->orWhere('name', 'Game Reviews')->orWhere('name', 'Game News')->orWhere('name', 'Mobile')->orderBy('name', 'ASC')->get();
        $features_1 = Category::with(['post.user'])->where('name', 'PC')->orWhere('name', 'Game News')->get();
        $slider = Post::take(8)->get();
        // $categories_sub_featured = Category::with(['post.user'])->orderBy('created_at', 'ASC')->take(2)->get();
        // $posts = Post::latest()->take(4)->get();
        // $mag_post = Post::orderBy('judul', 'DESC')->take(1)->get();

        // $nav_categories = Category::with(['post.user'])->get();

        return view('blog.landing', compact('categories', 'posts', 'navs', 'features_1', 'slider'));
    }

    public function isi_blog($slug)
    {
        Paginator::useBootstrap();
        $categories = Category::with(['post.user'])->get();
        $posts = Post::with(['category', 'tags', 'user'])->latest()->paginate(10);
        $post = Post::with(['category', 'tags', 'user'])->where('slug', $slug)->first();
        $navs = Category::with(['post.user'])->where('name', 'PC')->orWhere('name', 'Console')->orWhere('name', 'Game Reviews')->orWhere('name', 'Game News')->orWhere('name', 'Mobile')->orderBy('name', 'ASC')->get();
        // $categories_sub_featured = Category::with(['post.user'])->orderBy('created_at', 'ASC')->take(2)->get();
        // $posts = Post::latest()->take(4)->get();
        // $mag_post = Post::orderBy('judul', 'DESC')->take(1)->get();

        // $nav_categories = Category::with(['post.user'])->get();

        return view('blog.detail-post', compact('categories', 'posts', 'post', 'navs'));
    }

    public function kategori($slug)
    {
        Paginator::useBootstrap();
        $navs = Category::with(['post.user'])->where('name', 'PC')->orWhere('name', 'Console')->orWhere('name', 'Game Reviews')->orWhere('name', 'Game News')->orWhere('name', 'Mobile')->orderBy('name', 'ASC')->get();
        $categories = Category::with(['post.user'])->get();
        $category = Category::with(['post.user'])->where('slug', $slug)->first();
        $posts = Post::with(['category', 'tags', 'user'])->latest()->paginate(10);

        return view('blog.category', compact('category', 'posts', 'categories', 'navs'));
    }

    public function cari(Request $request)
    {
        Paginator::useBootstrap();
        $navs = Category::with(['post.user'])->where('name', 'PC')->orWhere('name', 'Console')->orWhere('name', 'Game Reviews')->orWhere('name', 'Game News')->orWhere('name', 'Mobile')->orderBy('name', 'ASC')->get();
        $categories = Category::with(['post.user'])->get();
        $post = Post::with(['category', 'tags', 'user'])->where('judul', 'LIKE', '%'.$request->cari.'%')->paginate(10);
        $posts = Post::with(['category', 'tags', 'user'])->latest()->paginate(10);

        return view('blog.cari', compact('navs', 'categories', 'post', 'posts', 'request'));
    }
}
