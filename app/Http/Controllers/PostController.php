<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Post;
use Alert;
use App\Models\Tags;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $posts = Post::with(['category', 'user'])->latest()->paginate(10);

        return view('admin.post.index', compact('posts'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $tags = Tags::all();

        return view('admin.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|max:64|string',
            'category_id' => 'required',
            'content' => 'required',
            'gambar' => 'required|image|max:4096|mimes:png,jpg,svg,gif,jpeg',
            'slug' => 'string|max:64',
        ]);

        $gambar = $request->gambar;
        $namaGambar = time().".".$gambar->getClientOriginalExtension();

        $gambar->move(public_path().'/uploads/posts', $namaGambar);

        $slug = Str::slug($request->judul);
        $post = Post::create([
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'gambar' => $namaGambar,
                'slug' => $slug,
                'user_id' => Auth::user()->id
            ]);

        $post->tags()->attach($request->tags);

        return redirect()->route('post.index')->withSuccess('Post successfully created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $tags = Tags::all();
        $categories = Category::orderBy('name', 'ASC')->get();

        return view('admin.post.edit', compact('post', 'tags', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $old_gambar = $post->gambar;
        $this->validate($request, [
            'judul' => 'required|max:64|string',
            'category_id' => 'required',
            'content' => 'required',
            'gambar' => 'image|max:4096|mimes:png,jpg,svg,gif,jpeg',
            'slug' => 'string|max:64'
        ]);

        //Cek apakah author/admin ingin memperbarui gambar
        if($request->has('gambar'))
        {
            $request->gambar->move(public_path().'/uploads/posts', $old_gambar);

            $slug = Str::slug($request->judul);
            $post_data = [
                    'judul' => $request->judul,
                    'category_id' => $request->category_id,
                    'content' => $request->content,
                    'gambar' => $old_gambar,
                    'slug' => $slug,
                    'user_id' => Auth::user()->id
                ];
        } else //Jika author/admin tidak ingin memperbarui gambar maka gambar tidak diupdate ke database
        {
            $slug = Str::slug($request->judul);
            $post_data = [
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'slug' => $slug,
                'user_id' => Auth::user()->id
            ];
        }

        $post->tags()->sync($request->tags);
        $post->update($post_data);

        return redirect()->route('post.index')->withSuccess('Post successfully updated !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        //Hapus Data Database
        $post->delete();

        return redirect()->route('post.index')->withSuccess('Post successfully deleted temporarily !');
    }

    public function delete_show()
    {
        $posts = Post::onlyTrashed()->latest()->paginate(10);

        return view('admin.post.delete', compact('posts'))->with('i');
    }

    public function restore($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->restore();

        return redirect()->route('post.deleted')->withSuccess('Post successfully restored !');
    }

    public function kill($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        //Cek jika ada gambar
        $file = public_path('/uploads/posts/').$post->gambar;
        if (file_exists($file))
        {
            //jika ada gambar maka hapus gambar
            @unlink($file);
        }
        $post->forceDelete();

        return redirect()->route('post.deleted')->withSuccess('Post successfully deleted permanently !');
    }
}
