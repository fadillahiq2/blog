<?php

namespace App\Http\Controllers;

use App\Models\Tags;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;

class TagController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $tags = Tags::latest()->paginate(10);

        return view('admin.tag.index', compact('tags'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'name' => 'required|min:2|max:20|string|unique:tags',
            'slug' => 'string|max:20'
        ]);

        $data['slug'] = Str::slug($request->name);

        Tags::create($data);

        return redirect()->route('tag.index')->withSuccess('Tag successfully created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(Tags $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tags::findOrFail($id);

        return view('admin.tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tags::findOrFail($id);

        $data = $request->all();

        $this->validate($request, [
            'name' => 'required|max:20|min:2|string|unique:tags,name,'.$tag->id,
            'slug' => 'string|max:20'
        ]);

        $data['slug'] = Str::slug($request->name);

        $tag->update($data);

        return redirect()->route('tag.index')->withSuccess('Tag successfully updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tags::findOrFail($id);
        $tag->delete();

        return redirect()->route('tag.index')->withSuccess('Tag successfully deleted !');
    }
}
