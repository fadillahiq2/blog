<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $users = User::where('id', '!=', Auth::user()->id)->latest()->paginate(10);
        return view('admin.user.index', compact('users'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|string|min:3',
            'email' => 'required|email|max:50|unique:users|min:3',
            'password' => 'required|max:50|min:8|confirmed',
            'password_confirmation' => 'same:password',
            'role' => 'required'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role
        ]);

        return redirect()->route('user.index')->withSuccess('User successfully created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|max:50|string|min:3',
            'email' => 'required|email|max:50|min:3|unique:users,email,'.$user->id,
            // 'current_password' => new MatchOldPassword,
            // 'new_password' => 'max:50|min:8',
            // 'new_confirm_password' => 'same:new_password',
            'role' => 'required'
        ]);

        // if($request->input('new_password') && $request->input('new_confirm_password') && $request->input('current_password'))
        // {
        //     $input_user = [
        //         'name' => $request->name,
        //         'email' => $request->email,
        //         'password' => Hash::make($request->new_password),
        //         'role' => $request->role
        //     ];
        // }else {
        //     $input_user = [
        //         'name' => $request->name,
        //         'email' => $request->email,
        //         'role' => $request->role
        //     ];
        // }

        $input_user = [
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role
        ];

        $user->update($input_user);

        return redirect()->route('user.index')->withSuccess('User successfully updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('user.index')->withSuccess('User successfully deleted !');
    }
}
