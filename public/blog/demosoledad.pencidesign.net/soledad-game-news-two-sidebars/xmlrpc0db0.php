<?xml version="1.0" encoding="UTF-8"?><rsd version="1.0" xmlns="http://archipelago.phrasewise.com/rsd">
	<service>
		<engineName>WordPress</engineName>
		<engineLink>https://wordpress.org/</engineLink>
		<homePageLink>https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars</homePageLink>
		<apis>
			<api name="WordPress" blogID="1" preferred="true" apiLink="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/xmlrpc.php" />
			<api name="Movable Type" blogID="1" preferred="false" apiLink="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/xmlrpc.php" />
			<api name="MetaWeblog" blogID="1" preferred="false" apiLink="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/xmlrpc.php" />
			<api name="Blogger" blogID="1" preferred="false" apiLink="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/xmlrpc.php" />
				<api name="WP-API" blogID="1" preferred="false" apiLink="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/wp-json/" />
			</apis>
	</service>
</rsd>
	