@extends('layouts.admin')

@section('title', 'Category')

@section('header')
    <h1>Category</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Edit Category</h4>
          <div class="card-header-action">
            <a href="{{ route('category.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
            @if($errors->any())
                <div class="alert alert-danger mx-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="mx-3 mt-2" action="{{ route('category.update', $category->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Please Insert Category Name" value="{{ $category->name }}" maxlength="50" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">Update Category</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
