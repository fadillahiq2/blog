@extends('layouts.admin')

@section('title', 'Post')

@section('header')
    <h1>Post</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>List Deleted Post</h4>
          <div class="card-header-action">
            <a href="{{ route('post.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>#</th>
                <th>Nama Post</th>
                <th>Slug Post</th>
                <th>Category</th>
                <th class="text-center">Tag</th>
                <th>Content</th>
                <th>Thumbnail</th>
                <th>Action</th>
              </tr>
              @forelse ($posts as $post)
              <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $post->judul }}</td>
                <td>{{ $post->slug }}</td>
                <td>{{ $post->category->name }}</td>
                <td>
                @foreach ($post->tags as $tag)
                    <ul>
                        <li>{{ $tag->name }}</li>
                    </ul>
                @endforeach
                </td>
                <td>{{ $post->content }}</td>
                <td><img src="{{ asset('uploads/posts/'.$post->gambar) }}" class="rounded img-thumbnail" width="300" alt="thumbnail"></td>
                <td>
                    <form action="{{ route('post.kill', $post->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a href="{{ route('post.restore', $post->id) }}" class="btn btn-warning">Restore</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                  <td colspan="12"><p class="text-center text-danger mt-3"><strong>Data Empty !</strong></p></td>
              </tr>
              @endforelse
            </table>
            {{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    @include('sweetalert::alert')
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@endpush
