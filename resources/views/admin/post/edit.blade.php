@extends('layouts.admin')

@section('title', 'Post')

@section('header')
    <h1>Post</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Edit Post</h4>
          <div class="card-header-action">
            <a href="{{ route('post.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
            @if($errors->any())
                <div class="alert alert-danger mx-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="mx-3 mt-2" action="{{ route('post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" id="judul" class="form-control" placeholder="Please Insert Judul" maxlength="64" required value="{{ $post->judul }}">
                </div>

                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select class="form-control select2" name="category_id" id="category_id" required>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if($category->id == $post->category_id) selected @endif>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tag_id">Tags</label>
                    <select class="form-control select2" name="tags[]" multiple="">
                        @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}"
                            @foreach ($post->tags as $value)
                                @if($tag->id == $value->id) selected @endif
                            @endforeach>{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="deskripsi">Content</label>
                    <textarea class="form-control deskripsi" name="content" id="deskripsi" cols="30" rows="10" placeholder="Please Insert Content" required>{{ $post->content }}</textarea>
                </div>

                <div class="form-group">
                    <label for="gambar">Thumbnail</label>
                    <input type="file" name="gambar" id="gambar" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">Update Post</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        var options = {
          filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
          filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
          filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
          filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
    </script>
    <script>
        CKEDITOR.replace( 'deskripsi', options);
    </script>
@endpush
