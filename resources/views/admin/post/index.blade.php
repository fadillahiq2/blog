@extends('layouts.admin')

@section('title', 'Post')

@section('header')
    <h1>Post</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>List Post</h4>
          <div class="card-header-action">
            <a href="{{ route('post.create') }}" class="btn btn-primary">Add Post <i class="fas fa-chevron-right"></i></a>
            <a href="{{ route('post.deleted') }}" class="btn btn-danger">Deleted Post <i class="fas fa-chevron-right"></i></a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>#</th>
                <th>Nama Post</th>
                <th>Slug Post</th>
                <th>Author</th>
                <th>Category</th>
                <th class="text-center">Tag</th>
                <th>Content</th>
                <th>Thumbnail</th>
                <th>Action</th>
              </tr>
              @forelse ($posts as $post)
              <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $post->judul }}</td>
                <td>{{ $post->slug }}</td>
                <td><strong>{{ $post->user->name }}</strong></td>
                <td>{{ $post->category->name }}</td>
                <td>
                @foreach ($post->tags as $tag)
                    <ul>
                        <h6><span class="badge badge-info">{{ $tag->name }}</span></h6>
                    </ul>
                @endforeach
                </td>
                <td>{!! Str::limit($post->content, 100) !!}</td>
                <td><img src="{{ asset('uploads/posts/'.$post->gambar) }}" class="rounded img-thumbnail my-2" width="300" alt="thumbnail"></td>
                <td>
                    <form class="d-flex" action="{{ route('post.destroy', $post->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a href="{{ route('post.edit', $post->id) }}" class="btn btn-warning mr-2">Edit</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                  <td colspan="12"><p class="text-center text-danger mt-3"><strong>Data Empty !</strong></p></td>
              </tr>
              @endforelse
            </table>
            {{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    @include('sweetalert::alert')
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@endpush
