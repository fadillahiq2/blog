@extends('layouts.admin')

@section('title', 'Tag')

@section('header')
    <h1>Tag</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Edit Tag</h4>
          <div class="card-header-action">
            <a href="{{ route('tag.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
            @if($errors->any())
                <div class="alert alert-danger mx-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="mx-3 mt-2" action="{{ route('tag.update', $tag->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="name">Tag Name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Please Insert Tag Name" value="{{ $tag->name }}" maxlength="20" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">Update Tag</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
