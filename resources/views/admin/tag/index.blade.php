@extends('layouts.admin')

@section('title', 'Tag')

@section('header')
    <h1>Tag</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>List Tag</h4>
          <div class="card-header-action">
            <a href="{{ route('tag.create') }}" class="btn btn-primary">Add Tag <i class="fas fa-chevron-right"></i></a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Action</th>
              </tr>
              @forelse ($tags as $tag)
              <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $tag->name }}</td>
                <td>{{ $tag->slug }}</td>
                <td>
                    <form action="{{ route('tag.destroy', $tag->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a href="{{ route('tag.edit', $tag->id) }}" class="btn btn-warning">Edit</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                  <td colspan="12"><p class="text-center text-danger mt-3"><strong>Data Empty !</strong></p></td>
              </tr>
              @endforelse
            </table>
            {{ $tags->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    @include('sweetalert::alert')
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@endpush
