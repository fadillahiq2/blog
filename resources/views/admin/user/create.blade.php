@extends('layouts.admin')

@section('title', 'User')

@section('header')
    <h1>User</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Add User</h4>
          <div class="card-header-action">
            <a href="{{ route('user.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
            @if($errors->any())
                <div class="alert alert-danger mx-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="mx-3 mt-2" action="{{ route('user.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Please Insert Name" maxlength="50" required value="{{ old('name') }}">
                    @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Please Insert Email" maxlength="50" required value="{{ old('email') }}">
                    @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Role User</label>
                    <select class="form-control select2" name="role" id="role">
                        <option value="" selected disabled>Pilih Role User</option>
                        <option value="0">Author</option>
                        <option value="1">Admin</option>
                    </select>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Please Insert Password" maxlength="50" required>
                        @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Please Insert Confirm Password" maxlength="50" required>
                    </div>
                </div>

                <div class="form-group form-check">
                    <input class="form-check-input" id="showpw" type="checkbox" onclick="showPw()">
                    <label class="form-check-label" for="showpw">Show Password</label>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save User</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
  <script>
    function showPw() {
        var x = document.getElementById("password");
        var y = document.getElementById("password_confirmation");
        if (x.type && y.type === "password") {
            x.type = "text";
            y.type = "text";
        } else {
            x.type = "password";
            y.type = "password";
        }
    }
  </script>
@endpush
