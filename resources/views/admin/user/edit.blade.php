@extends('layouts.admin')

@section('title', 'User')

@section('header')
    <h1>User</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Add User</h4>
          <div class="card-header-action">
            <a href="{{ route('user.index') }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</a>
          </div>
        </div>
        <div class="card-body p-0">
            @if($errors->any())
                <div class="alert alert-danger mx-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="mx-3 mt-2" action="{{ route('user.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Please Insert Name" maxlength="50" required value="{{ $user->name }}">
                    @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Please Insert Email" maxlength="50" required value="{{ $user->email }}">
                    @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Role User</label>
                    <select class="form-control select2" name="role" id="role">
                        <option value="0" @if($user->role == 0) selected @endif>Author</option>
                        <option value="1" @if($user->role == 1) selected @endif>Admin</option>
                    </select>
                </div>

                {{-- <div class="form-group">
                    <label for="current_password">Old Password</label>
                    <input type="password" name="current_password" id="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Please Insert Current Password" maxlength="50">
                    @error('current_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="new_password">Password</label>
                        <input type="password" name="new_password" id="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="Please Insert New Password" maxlength="50">
                        @error('new_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="new_confirm_password">Confirm Password</label>
                        <input type="password" name="new_confirm_password" id="new_confirm_password" class="form-control" placeholder="Please Insert New Confirm Password" maxlength="50">
                    </div>
                </div> --}}

                {{-- <div class="form-group form-check">
                    <input class="form-check-input" id="showpw" type="checkbox" onclick="showPw()">
                    <label class="form-check-label" for="showpw">Show Password</label>
                </div> --}}


                <div class="form-group">
                    <button type="submit" class="btn btn-warning">Update User</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
  <script>
    function showPw() {
        var x = document.getElementById("new_password");
        var y = document.getElementById("new_confirm_password");
        var z = document.getElementById("current_password");
        if (x.type && y.type && z.type === "password") {
            x.type = "text";
            y.type = "text";
            z.type = "text";
        } else {
            x.type = "password";
            y.type = "password";
            z.type = "password";
        }
    }
  </script>
@endpush
