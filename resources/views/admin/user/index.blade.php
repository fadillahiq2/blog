@extends('layouts.admin')

@section('title', 'User')

@section('header')
    <h1>User</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>List User</h4>
          <div class="card-header-action">
            <a href="{{ route('user.create') }}" class="btn btn-primary">Add User <i class="fas fa-chevron-right"></i></a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>#</th>
                <th>Nama User</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
              </tr>
              @forelse ($users as $user)
              <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if($user->role == 1)
                        <span class="badge badge-success">Admin</span>
                    @else
                        <span class="badge badge-info">Author</span>
                    @endif
                </td>
                <td>
                    <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning">Edit</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                  <td colspan="12"><p class="text-center text-danger mt-3"><strong>Data Empty !</strong></p></td>
              </tr>
              @endforelse
            </table>
            {{ $users->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    @include('sweetalert::alert')
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@endpush
