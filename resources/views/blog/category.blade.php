@extends('layouts.blog')

@section('content')
<section
class="penci-section penci-enSticky penci-repons-elsection penci-sb2_con_sb1 penci-structure-30 elementor-section elementor-top-section elementor-element elementor-element-d5afb26 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
data-id="d5afb26" data-element_type="section">
<div class="elementor-container elementor-column-gap-wider">
    <div class="elementor-row">
        <div class="penci-ercol-33 penci-ercol-order-1 penci-sticky-sb penci-sidebarSC elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-e381f09"
    data-id="e381f09" data-element_type="column">
    <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
        <div class="elementor-widget-wrap">
            <div class="elementor-element elementor-element-1240cd5 elementor-widget elementor-widget-penci-facebook-page"
                data-id="1240cd5" data-element_type="widget" data-widget_type="penci-facebook-page.default">
                <div class="elementor-widget-container">
                    <div class="penci-block-vc penci_recent-posts-sc">
                        <div
                            class="penci-border-arrow penci-homepage-title penci-home-latest-posts style-12 pcalign-left block-title-icon-left">
                            <h3 class="inner-arrow">
                                <span>Popular (not working)</a>
                            </h3>
                        </div>
                        <div class="penci-block_content">
                            <ul id="penci-latestwg-6960" class="side-newsfeed">
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/1-263x175.jpg"
                                                href="#"
                                                title="Fortnite Chapter 2 Trailer Leaks, Shows New Map and Moren This Year"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="Fortnite Chapter 2 Trailer Leaks, Shows New Map and Moren This Year">
                                                    Fortnite Chapter 2 Trailer
                                                    Leaks, Shows New Map and
                                                    Moren This Year </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/2-263x175.jpg"
                                                href="#"
                                                title="Apple Arcade Adds 5 More Games to iOS, Now Boasts 80 Titles Quality Game"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="Apple Arcade Adds 5 More Games to iOS, Now Boasts 80 Titles Quality Game">
                                                    Apple Arcade Adds 5 More
                                                    Games to iOS, Now Boasts 80
                                                    Titles Quality Game </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/3-263x175.jpg"
                                                href="#"
                                                title="Here&#8217;s How the PS5&#8217;s Hardware Compares to the PS4&#8217;s In Term Of Performance"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="Here&#8217;s How the PS5&#8217;s Hardware Compares to the PS4&#8217;s In Term Of Performance">
                                                    Here&#8217;s How the
                                                    PS5&#8217;s Hardware
                                                    Compares to the PS4&#8217;s
                                                    In Term Of Performance </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/4-263x175.jpg"
                                                href="#"
                                                title="The First 20 Minutes of The Outer Worlds PC Version Coming This Winter"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="The First 20 Minutes of The Outer Worlds PC Version Coming This Winter">
                                                    The First 20 Minutes of The
                                                    Outer Worlds PC Version
                                                    Coming This Winter </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <div class="penci-piechart penci-piechart-small" data-percent="67"
                                                data-color="#ff4081" data-trackcolor="rgba(0, 0, 0, .2)" data-size="34"
                                                data-thickness="2">
                                                <span class="penci-chart-text">6.7</span>
                                            </div>
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/5-263x175.jpg"
                                                href="#"
                                                title="The Witcher 3 Nintendo Switch Detailed Review With Graphic &#038; Performance"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="The Witcher 3 Nintendo Switch Detailed Review With Graphic &#038; Performance">
                                                    The Witcher 3 Nintendo
                                                    Switch Detailed Review With
                                                    Graphic &#038; Performance
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-0def931 elementor-widget elementor-widget-penci-mail-chimp"
                data-id="0def931" data-element_type="widget" data-widget_type="penci-mail-chimp.default">
                <div class="elementor-widget-container">
                    <div class="penci-block-vc penci-mailchimp-block penci-mailchimp-s1">
                        <div
                            class="penci-border-arrow penci-homepage-title penci-home-latest-posts style-12 pcalign-left block-title-icon-left">
                            <h3 class="inner-arrow">
                                <span>Newsletter (not working)</a>
                            </h3>
                        </div>
                        <div class="penci-block_content">
                            <div class="widget widget_mc4wp_form_widget">
                                <script>(function () {
                                        window.mc4wp = window.mc4wp || {
                                            listeners: [],
                                            forms: {
                                                on: function (evt, cb) {
                                                    window.mc4wp.listeners.push(
                                                        {
                                                            event: evt,
                                                            callback: cb
                                                        }
                                                    );
                                                }
                                            }
                                        }
                                    })();
                                </script>
                                <!-- Mailchimp for WordPress v4.8.3 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                                <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-299" method="post" data-id="299"
                                    data-name="">
                                    <div class="mc4wp-form-fields">
                                        <p class="mdes">Subscribe our newsletter
                                            for latest news, service & promo.
                                            Let's stay updated!</p>
                                        <p class="mname"><input type="text" name="NAME" placeholder="Name..." /></p>
                                        <p class="memail"><input type="email" id="mc4wp_email" name="EMAIL"
                                                placeholder="Email..." required /></p>
                                        <p class="msubmit"><input type="submit" value="Subscribe" /></p>
                                    </div><label style="display: none !important;">Leave
                                        this field empty if you're human: <input type="text" name="_mc4wp_honeypot"
                                            value="" tabindex="-1" autocomplete="off" /></label><input type="hidden"
                                        name="_mc4wp_timestamp" value="1625676895" /><input type="hidden"
                                        name="_mc4wp_form_id" value="299" /><input type="hidden"
                                        name="_mc4wp_form_element_id" value="mc4wp-form-1" />
                                    <div class="mc4wp-response"></div>
                                </form><!-- / Mailchimp for WordPress Plugin -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="penci-ercol-33 penci-ercol-order-2 penci-sticky-sb penci-sidebarSC elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-32446a1"
    data-id="32446a1" data-element_type="column">
    <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
        <div class="elementor-widget-wrap">
            <div class="elementor-element elementor-element-b7823a9 elementor-widget elementor-widget-penci-latest-posts"
                data-id="b7823a9" data-element_type="widget"
                data-settings="{&quot;order_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}"
                data-widget_type="penci-latest-posts.default">
                <div class="elementor-widget-container">
                    <div id="pencilatest_posts_68264"
                        class="penci-latest-posts-sc penci-latest-posts-list penci-latest-posts-el penci-el-mixed-s1 penci-latest-posts-left penci-std-continue-center penci-std-excerpt-center penci-grid-excerpt-left">
                        <div class="archive-box">
                            <div class="title-bar">
                                <span>Category:</span> 						<h1>{{ $category->name }}</h1>
                            </div>
                        </div>

                        <div class="penci-wrapper-posts-content">

                            <ul class="penci-wrapper-data penci-grid penci-shortcode-render">
                                @foreach ($category->post->take(10) as $latest_game_news)
                                <li class="list-post penci-item-listp">
                                    <article id="post-102" class="item hentry">
                                        <div class="thumbnail">

                                            <a class="penci-image-holder penci-lazy"
                                                data-src="{{ asset('uploads/posts/'.$latest_game_news->gambar) }}"
                                                href="{{ route('isi_blog', $latest_game_news->slug) }}" title="{{ $latest_game_news->judul }}">
                                            </a>

                                        </div>

                                        <div class="content-list-right content-list-center">
                                            <div class="header-list-style">
                                                <span class="cat">
                                                    @foreach ($latest_game_news->tags as $tag)
                                                    <a class="penci-cat-name penci-cat-7" href="#" rel="category tag">{{
                                                        $tag->name }}</a>
                                                    @endforeach
                                                </span>

                                                <h2 class="penci-entry-title entry-title grid-title">
                                                    <a href="{{ route('isi_blog', $latest_game_news->slug) }}">{{ $latest_game_news->judul }}</a>
                                                </h2>
                                                <div class="penci-hide-tagupdated">
                                                    <span class="author-italic author vcard">by
                                                        <a class="url fn n" href="#">{{ $latest_game_news->user->name }}</a></span>
                                                    <time class="entry-date published"
                                                        datetime="{{ \Carbon\Carbon::parse($latest_game_news->created_at)->isoFormat('D MMMM Y') }}">{{
                                                        \Carbon\Carbon::parse($latest_game_news->created_at)->isoFormat('D
                                                        MMMM Y') }}</time>
                                                </div>
                                                <div class="grid-post-box-meta">
                                                    <span class="otherl-date-author author-italic author vcard">by
                                                        <a class="url fn n" href="#">{{ $latest_game_news->user->name }}</a></span>
                                                    <span class="otherl-date"><time class="entry-date published"
                                                            datetime="{{ \Carbon\Carbon::parse($latest_game_news->created_at)->isoFormat('D MMMM Y') }}">{{
                                                            \Carbon\Carbon::parse($latest_game_news->created_at)->isoFormat('D
                                                            MMMM Y') }}</time></span>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                @endforeach
                            </ul>

                            <div class="penci-pagination align-left">
                                <ul class='page-numbers'>
                                    {{ $posts->links() }}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <style>
                        #pencilatest_posts_68264 .list-post .header-list-style:after,
                        #pencilatest_posts_68264 .grid-header-box:after,
                        #pencilatest_posts_68264 .penci-overlay-over .overlay-header-box:after,
                        #pencilatest_posts_68264 .home-featured-cat-content .first-post .magcat-detail .mag-header:after {
                            content: none;
                        }

                        #pencilatest_posts_68264 .list-post .header-list-style,
                        #pencilatest_posts_68264 .grid-header-box,
                        #pencilatest_posts_68264 .penci-overlay-over .overlay-header-box,
                        #pencilatest_posts_68264 .home-featured-cat-content .first-post .magcat-detail .mag-header {
                            padding-bottom: 0;
                        }
                    </style>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="penci-ercol-33 penci-ercol-order-3 penci-sticky-sb penci-sidebarSC elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-f5d1e64"
    data-id="f5d1e64" data-element_type="column">
    <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
        <div class="elementor-widget-wrap">
            <div class="elementor-element elementor-element-9c99a10 elementor-widget elementor-widget-penci-social-media"
                data-id="9c99a10" data-element_type="widget" data-widget_type="penci-social-media.default">
                <div class="elementor-widget-container">
                    <div class="penci-block-vc penci-social-media">
                        <div
                            class="penci-border-arrow penci-homepage-title penci-home-latest-posts style-12 pcalign-left block-title-icon-left">
                            <h3 class="inner-arrow">
                                <span>Social Connect</a>
                            </h3>
                        </div>
                        <div class="penci-block_content widget-social pc_alignleft show-text">
                            <a href="https://www.facebook.com/sky.lord.high" aria-label="Facebook" rel="noreferrer"
                                target="_blank"><i class="penci-faicon fa fa-facebook"></i><span>Facebook</span></a>
                            <a href="https://www.instagram.com/fadillahiq" aria-label="Instagram" rel="noreferrer" target="_blank"><i
                                    class="penci-faicon fa fa-instagram"></i><span>Instagram</span></a>
                            <a href="https://www.linkedin.com/in/fadillahiq" aria-label="LinkedIn" rel="noreferrer" target="_blank"><i
                                    class="penci-faicon fa fa-linkedin"></i><span>LinkedIn</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-7c91887 elementor-widget elementor-widget-penci-recent-posts"
                data-id="7c91887" data-element_type="widget"
                data-settings="{&quot;rp_rows_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}"
                data-widget_type="penci-recent-posts.default">
                <div class="elementor-widget-container">
                    @foreach ($categories->where('name', 'Game Reviews')->take(5) as $game_reviews)
                    <div class="penci-block-vc penci_recent-posts-sc">
                        <div
                            class="penci-border-arrow penci-homepage-title penci-home-latest-posts style-12 pcalign-left block-title-icon-left">
                            <h3 class="inner-arrow">
                                <span>{{ $game_reviews->name }}</a>
                            </h3>
                        </div>
                        <div class="penci-block_content">
                            <ul id="penci-latestwg-3457" class="side-newsfeed">
                                @foreach ($game_reviews->post as $game_reviews_post)
                                <li class="penci-feed">
                                    <div class="side-item">
                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="{{ asset('uploads/posts/'.$game_reviews_post->gambar) }}"
                                                href="{{ route('isi_blog', $game_reviews_post) }}" title="{{ $game_reviews_post->judul }}"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="{{ route('isi_blog', $game_reviews_post->slug) }}"
                                                    rel="bookmark" title="{{ $game_reviews_post->judul }}">{{ $game_reviews_post->judul }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="elementor-element elementor-element-c82fffe elementor-widget elementor-widget-penci-popular-cat"
                data-id="c82fffe" data-element_type="widget" data-widget_type="penci-popular-cat.default">
                <div class="elementor-widget-container">
                    <div class="penci-block-vc penci-block-popular-cat widget_categories widget widget_categories">
                        <div
                            class="penci-border-arrow penci-homepage-title penci-home-latest-posts style-12 pcalign-left block-title-icon-left">
                            <h3 class="inner-arrow">
                                <span>Categories</a>
                            </h3>
                        </div>
                        <div class="penci-block_content penci-div-inner">
                            <ul>
                                @foreach ($categories as $right_category)
                                <li class="cat-item cat-item-8"><a href="{{ route('kategori', $right_category->slug) }}">{{ $right_category->name }}<span
                                            class="category-item-count">{{ $right_category->post->count() }}</span></a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-71281c1 elementor-widget elementor-widget-penci-video-playlist"
                data-id="71281c1" data-element_type="widget" data-widget_type="penci-video-playlist.default">
                <div class="elementor-widget-container">
                    <div class="penci-block-vc penci-video_playlist pencisc-column-1">
                        <div class="penci-block_content">
                            <img onclick="location.href='https://my.idcloudhost.com/aff.php?aff=7951'"
                                src="{{ asset('uploads/images/IDCloudHost-SSD-Cloud-Hosting-Indonesia-240x400.jpg') }}"
                                height="240" width="400" border="0" alt="IDCloudHost | SSD Cloud Hosting Indonesia" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
</section>
@endsection
