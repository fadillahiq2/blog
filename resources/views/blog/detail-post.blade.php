@extends('layouts.detail')
@section('image-content')
<div class="post-image  penci-move-title-above">
    <a href="../../../net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/9.jpg"
        data-rel="penci-gallery-bground-content"><span
            class="attachment-penci-full-thumb size-penci-full-thumb penci-single-featured-img wp-post-image penci-lazy"
            data-src="{{ asset('uploads/posts/'.$post->gambar) }}"
            style="padding-top: 60.9402%;"></span></a>
    <div class="standard-post-special_wrapper two-sidebar">
        <div class="header-standard header-classic single-header">
            <div class="penci-standard-cat penci-single-cat"><span class="cat"><a
                        class="penci-cat-name penci-cat-7" href="#"
                        rel="category tag">{{ $post->category->name }}</a></span></div>

            <h1 class="post-title single-post-title entry-title">{{ $post->judul }}</h1>
            <div class="penci-hide-tagupdated">
                <span class="author-italic author vcard">by <a class="url fn n"
                        href="../author/pencidesign/index.html">{{ $post->user->name }}</a></span>
                <time class="entry-date published" datetime="{{ \Carbon\Carbon::parse($post->created_at)->isoFormat('D MMM Y') }}">{{ \Carbon\Carbon::parse($post->created_at)->isoFormat('D MMM Y') }}</time>
            </div>
            <div class="post-box-meta-single">
                <span class="author-post byline"><span class="author vcard">by <a
                            class="author-url url fn n" href="../author/pencidesign/index.html">{{ $post->user->name }}</a></span></span>
                <span><time class="entry-date published" datetime="{{ \Carbon\Carbon::parse($post->created_at)->isoFormat('D MMM Y') }}">{{ \Carbon\Carbon::parse($post->created_at)->isoFormat('D MMM Y') }}</time></span>
                <span>0 comment</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div id="main" class="penci-main-sticky-sidebar">
    <div class="theiaStickySidebar">
        <article id="post-102" class="post type-post status-publish">


            <div class="post-entry blockquote-style-1">
                <div class="inner-post-entry entry-content" id="penci-post-entry-inner">
                    <p>{!! $post->content !!}</p>
                    <div class="penci-single-link-pages">
                    </div>
                    <div class="post-tags">
                        @foreach ($post->tags as $tag)
                        <a href="../tag/game/index.html" rel="tag">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>


            {{-- <div class="tags-share-box tags-share-box-2_3 tags-share-box-s2">
                <span class="penci-social-share-text">Share</span>
                <div class="post-share">
                    <span class="post-share-item post-share-plike">
                        <span class="count-number-like">1</span><a href="#" aria-label="Like this post"
                            class="penci-post-like single-like-button" data-post_id="102" title="Like"
                            data-like="Like" data-unlike="Unlike"><i
                                class="penci-faicon fa fa-heart-o"></i></a> </span>
                    <div class="list-posts-share"><a class="post-share-item post-share-facebook"
                            aria-label="Share on Facebook" target="_blank" rel="noreferrer"
                            href="https://www.facebook.com/sharer/sharer.php?u=https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/call-of-duty-dev-says-there-will-be-no-loot-boxes-in-modern-warfare/"><i
                                class="penci-faicon fa fa-facebook"></i><span
                                class="dt-share">Facebook</span></a><a
                            class="post-share-item post-share-twitter" aria-label="Share on Twitter"
                            target="_blank" rel="noreferrer"
                            href="https://twitter.com/intent/tweet?text=Check%20out%20this%20article:%20Call%20of%20Duty%20Dev%20Says%20There%20Will%20Be%20No%20Loot%20Boxes%20in%20Modern%20Warfare%20-%20https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/call-of-duty-dev-says-there-will-be-no-loot-boxes-in-modern-warfare/"><i
                                class="penci-faicon fa fa-twitter"></i><span
                                class="dt-share">Twitter</span></a><a
                            class="post-share-item post-share-pinterest" aria-label="Pin to Pinterest"
                            data-pin-do="none" rel="noreferrer"
                            onclick="var e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','../../../assets.pinterest.com/js/pinmarklet2a3b.js?r='+Math.random()*99999999);document.body.appendChild(e);"><i
                                class="penci-faicon fa fa-pinterest"></i><span
                                class="dt-share">Pinterest</span></a><a
                            class="post-share-item post-share-email" target="_blank"
                            aria-label="Share via Email" rel="noreferrer"
                            href="https://demosoledad.pencidesign.net/cdn-cgi/l/email-protection#8eb1fdfbece4ebedfab3cdefe2e2abbcbee1e8abbcbecafbfaf7abbcbecaebf8abbcbeddeff7fdabbcbedae6ebfcebabbcbed9e7e2e2abbcbeccebabbcbec0e1abbcbec2e1e1faabbcbecce1f6ebfdabbcbee7e0abbcbec3e1eaebfce0abbcbed9effce8effceba8adbebdb6b5ccc1cad7b3e6fafafefdb4a1a1eaebe3e1fde1e2ebeaefeaa0feebe0ede7eaebfde7e9e0a0e0ebfaa1fde1e2ebeaefeaa3e9efe3eba3e0ebf9fda3faf9e1a3fde7eaebeceffcfda1edefe2e2a3e1e8a3eafbfaf7a3eaebf8a3fdeff7fda3fae6ebfceba3f9e7e2e2a3eceba3e0e1a3e2e1e1faa3ece1f6ebfda3e7e0a3e3e1eaebfce0a3f9effce8effceba1"><i
                                class="penci-faicon fa fa-envelope"></i><span
                                class="dt-share">Email</span></a></div>
                </div>
            </div>
            <div class="post-author">
                <div class="author-img">
                    <img alt=''
                        src='https://secure.gravatar.com/avatar/2fe1531e5dccb7dac56b5dc9f9016d5e?s=100&amp;d=mm&amp;r=g'
                        srcset="https://secure.gravatar.com/avatar/2fe1531e5dccb7dac56b5dc9f9016d5e?s=200&#038;d=mm&#038;r=g 2x"
                        class='avatar avatar-100 photo' height='100' width='100' loading='lazy' />
                </div>
                <div class="author-content">
                    <h5><a href="../author/pencidesign/index.html" title="Author Penci Design"
                            rel="author">Penci Design</a></h5>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                        veritatis et quasi architecto beatae vitae. Nemo enim ipsam voluptatem.</p>
                    <a rel="noreferrer" target="_blank" class="author-social" href="http://#"><i
                            class="penci-faicon fa fa-globe"></i></a>
                    <a rel="noreferrer" target="_blank" class="author-social"
                        href="https://facebook.com/#"><i class="penci-faicon fa fa-facebook"></i></a>
                    <a rel="noreferrer" target="_blank" class="author-social"
                        href="https://twitter.com/#"><i class="penci-faicon fa fa-twitter"></i></a>
                    <a rel="noreferrer" target="_blank" class="author-social"
                        href="https://instagram.com/#"><i class="penci-faicon fa fa-instagram"></i></a>
                    <a rel="noreferrer" target="_blank" class="author-social"
                        href="https://pinterest.com/#"><i class="penci-faicon fa fa-pinterest"></i></a>
                    <a rel="noreferrer" target="_blank" class="author-social"
                        href="http://#.tumblr.com/"><i class="penci-faicon fa fa-tumblr"></i></a>
                </div>
            </div>
            <div class="post-pagination">
                <div class="prev-post">
                    <a class="penci-post-nav-thumb penci-holder-load penci-lazy"
                        href="../ghostbusters-the-video-game-remastered-review-with-updated-storyline/index.html"
                        data-src="../../../net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/10-150x150.jpg">
                    </a>
                    <div class="prev-post-inner">
                        <div class="prev-post-title">
                            <span>previous post</span>
                        </div>
                        <a
                            href="../ghostbusters-the-video-game-remastered-review-with-updated-storyline/index.html">
                            <div class="pagi-text">
                                <h5 class="prev-title">Ghostbusters: The Video Game Remastered Review
                                    With Storyline</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="next-post">
                    <a class="penci-post-nav-thumb penci-holder-load penci-lazy nav-thumb-next"
                        href="../ghostbusters-the-video-game-remastered-review-with-updated-storyline/index.html"
                        data-src="../../../net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/10-150x150.jpg">
                    </a>
                    <div class="next-post-inner">
                        <div class="prev-post-title">
                            <span>previous post</span>
                        </div>
                        <a
                            href="../ghostbusters-the-video-game-remastered-review-with-updated-storyline/index.html">
                            <div class="pagi-text">
                                <h5 class="next-title">Ghostbusters: The Video Game Remastered Review
                                    With Storyline</h5>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
            <div class="post-related">
                <div class="post-title-box">
                    <h4 class="post-box-title">You may also like</h4>
                </div>
                <div class="penci-owl-carousel penci-owl-carousel-slider penci-related-carousel"
                    data-lazy="true" data-item="3" data-desktop="3" data-tablet="2" data-tabsmall="2"
                    data-auto="false" data-speed="300" data-dots="true" data-nav="false">
                    @foreach ($posts->take(9) as $you_may_also_like)
                    <div class="item-related">
                        <a class="related-thumb penci-image-holder owl-lazy"
                            data-src="{{ asset('uploads/posts/'.$you_may_also_like->gambar) }}"
                            href="#"
                            title="{{ $you_may_also_like->judul }}">
                        </a>
                        <h3><a
                                href="#">{{ $you_may_also_like->judul }}</a></h3>
                        <span class="date"><time class="entry-date published"
                                datetime="2019-10-15T07:45:16+00:00">{{ \Carbon\Carbon::parse($you_may_also_like->created_at)->isoFormat('D MMM Y') }}</time></span>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="post-comments no-comment-yet penci-comments-hide-0" id="comments">
                <div id="respond" class="comment-respond">
                    <h3 id="reply-title" class="comment-reply-title"><span>Leave a Comment</span>
                        <small><a rel="nofollow" id="cancel-comment-reply-link"
                                href="index.html#respond" style="display:none;">Cancel Reply</a></small>
                    </h3>
                    <form
                        action="https://demosoledad.pencidesign.net/soledad-game-news-two-sidebars/wp-comments-post.php"
                        method="post" id="commentform" class="comment-form">
                        <p class="comment-form-comment"><textarea id="comment" name="comment" cols="45"
                                rows="8" placeholder="Your Comment" aria-required="true"></textarea></p>
                        <p class="comment-form-author"><input id="author" name="author" type="text"
                                value="" placeholder="Name*" size="30" aria-required='true' /></p>
                        <p class="comment-form-email"><input id="email" name="email" type="text"
                                value="" placeholder="Email*" size="30" aria-required='true' /></p>
                        <p class="comment-form-url"><input id="url" name="url" type="text" value=""
                                placeholder="Website" size="30" /></p>
                        <p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent"
                                name="wp-comment-cookies-consent" type="checkbox" value="yes" /><span
                                class="comment-form-cookies-text" for="wp-comment-cookies-consent">Save
                                my name, email, and website in this browser for the next time I
                                comment.</span></p>
                        <p class="form-submit"><input name="submit" type="submit" id="submit"
                                class="submit" value="Submit" /> <input type='hidden'
                                name='comment_post_ID' value='102' id='comment_post_ID' />
                            <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                        </p>
                    </form>
                </div><!-- #respond -->
            </div> <!-- end comments div --> --}}
        </article>
    </div>
</div>
@endsection
