@extends('layouts.blog')

@section('content')
<section
class="penci-section penci-disSticky penci-structure-10 elementor-section elementor-top-section elementor-element elementor-element-11054dd elementor-section-boxed elementor-section-height-default elementor-section-height-default"
data-id="11054dd" data-element_type="section"
data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;pyramids&quot;}">
@include('includes_frontend.slider')
</section>
<section
class="penci-section penci-disSticky penci-structure-20 elementor-section elementor-top-section elementor-element elementor-element-6b1faef elementor-section-boxed elementor-section-height-default elementor-section-height-default"
data-id="6b1faef" data-element_type="section">
@include('includes_frontend.featured-1')
</section>
<section
class="penci-section penci-disSticky penci-structure-20 elementor-section elementor-top-section elementor-element elementor-element-e53e164 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
data-id="e53e164" data-element_type="section"
data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;,&quot;shape_divider_bottom&quot;:&quot;pyramids&quot;}">
@include('includes_frontend.featured-2')
</section>
<section
class="penci-section penci-enSticky penci-repons-elsection penci-sb2_con_sb1 penci-structure-30 elementor-section elementor-top-section elementor-element elementor-element-d5afb26 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
data-id="d5afb26" data-element_type="section">
<div class="elementor-container elementor-column-gap-wider">
    <div class="elementor-row">
        @include('includes_frontend.sidebar')
    </div>
</div>
</section>
@endsection
