<footer class="main-footer">
    <div class="footer-left">
      Copyright &copy; 2021 <i class="fas fa-heart" style="color: red;"></i> Develop by <a class="text-decoration-none" href="https://linkedin.com/in/fadillahiq" target="_blank">Muhamad Iqbal Fadilah</a> and <a class="text-decoration-none" href="https://www.linkedin.com/in/wildan-ardiansyah/" target="_blank">Muhammad Wildan Ardiansyah</a>
    </div>
  </footer>
