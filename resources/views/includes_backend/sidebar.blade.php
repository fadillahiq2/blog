<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">Blog SEO</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">BS</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="{{ (request()->is('admin/home')) ? 'active' : ''}}"><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        <li class="menu-header">Menu</li>
        <li class="{{ (request()->is('admin/category*')) ? 'active' : ''}}"><a class="nav-link" href="{{ route('category.index') }}"><i class="far fa-clipboard"></i> <span>Category</span></a></li>
        <li class="{{ (request()->is('admin/tag*')) ? 'active' : ''}}"><a class="nav-link" href="{{ route('tag.index') }}"><i class="far fa-bookmark"></i> <span>Tag</span></a></li>
        <li class="{{ (request()->is('admin/post*')) ? 'active' : ''}}"><a class="nav-link" href="{{ route('post.index') }}"><i class="fas fa-book-open"></i> <span>Post</span></a></li>
        <li class="{{ (request()->is('admin/user*')) ? 'active' : ''}}"><a class="nav-link" href="{{ route('user.index') }}"><i class="far fa-user"></i> <span>Users</span></a></li>
      </ul>
  </aside>
