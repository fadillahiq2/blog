<div class="footer-widget-wrapper footer-widget-style-4">
    <aside id="penci_latest_news_widget-2" class="widget penci_latest_news_widget">
        <h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Recent Posts</span></h4>
        <ul id="penci-latestwg-1758" class="side-newsfeed">
            @foreach ($posts->take(3) as $bottom_recent_post)
            <li class="penci-feed">
                <div class="side-item">
                    <div class="side-image">
                        <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                            data-src="{{ asset('uploads/posts/'.$bottom_recent_post->gambar) }}" href="#"
                            title="{{ $bottom_recent_post->judul }}"></a>

                    </div>
                    <div class="side-item-text">
                        <h4 class="side-title-post">
                            <a href="{{ route('isi_blog', $bottom_recent_post->slug) }}" rel="bookmark" title="{{ $bottom_recent_post->judul }}">
                                {{ $bottom_recent_post->judul }} </a>
                        </h4>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </aside>
</div>
<div class="footer-widget-wrapper footer-widget-style-4">
    <aside id="penci_latest_news_widget-3" class="widget penci_latest_news_widget">
        <h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Popular (not working)</span>
        </h4>
        <ul id="penci-latestwg-7669" class="side-newsfeed">
            <li class="penci-feed">
                <div class="side-item">

                    <div class="side-image">
                        <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                            data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/1-263x175.jpg"
                            href="fortnite-chapter-2-trailer-leaks-shows-new-map-and-moren-this-year/index.html"
                            title="Fortnite Chapter 2 Trailer Leaks, Shows New Map and Moren This Year"></a>

                    </div>
                    <div class="side-item-text">
                        <h4 class="side-title-post">
                            <a href="fortnite-chapter-2-trailer-leaks-shows-new-map-and-moren-this-year/index.html"
                                rel="bookmark"
                                title="Fortnite Chapter 2 Trailer Leaks, Shows New Map and Moren This Year">
                                Fortnite Chapter 2 Trailer Leaks, Shows New Map and Moren This Year </a>
                        </h4>
                    </div>
                </div>
            </li>
            <li class="penci-feed">
                <div class="side-item">

                    <div class="side-image">
                        <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                            data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/2-263x175.jpg"
                            href="apple-arcade-adds-5-more-games-to-ios-now-boasts-80-titles/index.html"
                            title="Apple Arcade Adds 5 More Games to iOS, Now Boasts 80 Titles Quality Game"></a>

                    </div>
                    <div class="side-item-text">
                        <h4 class="side-title-post">
                            <a href="apple-arcade-adds-5-more-games-to-ios-now-boasts-80-titles/index.html"
                                rel="bookmark"
                                title="Apple Arcade Adds 5 More Games to iOS, Now Boasts 80 Titles Quality Game">
                                Apple Arcade Adds 5 More Games to iOS, Now Boasts 80 Titles Quality Game
                            </a>
                        </h4>
                    </div>
                </div>
            </li>
            <li class="penci-feed">
                <div class="side-item">

                    <div class="side-image">
                        <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                            data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/3-263x175.jpg"
                            href="heres-how-the-ps5s-hardware-compares-to-the-ps4s-in-term-of-performance/index.html"
                            title="Here&#8217;s How the PS5&#8217;s Hardware Compares to the PS4&#8217;s In Term Of Performance"></a>

                    </div>
                    <div class="side-item-text">
                        <h4 class="side-title-post">
                            <a href="heres-how-the-ps5s-hardware-compares-to-the-ps4s-in-term-of-performance/index.html"
                                rel="bookmark"
                                title="Here&#8217;s How the PS5&#8217;s Hardware Compares to the PS4&#8217;s In Term Of Performance">
                                Here&#8217;s How the PS5&#8217;s Hardware Compares to the PS4&#8217;s In
                                Term Of Performance </a>
                        </h4>
                    </div>
                </div>
            </li>
        </ul>
    </aside>
</div>
<div class="footer-widget-wrapper footer-widget-style-4">
    @foreach ($categories->where('name', 'Game News') as $bottom_game_news)
    <aside id="penci_latest_news_widget-4" class="widget penci_latest_news_widget">
        <h4 class="widget-title penci-border-arrow"><span class="inner-arrow">{{ $bottom_game_news->name }}</span></h4>
        <ul id="penci-latestwg-5419" class="side-newsfeed">
            @foreach ($bottom_game_news->post->take(3) as $bottom_game_news_post)
            <li class="penci-feed">
                <div class="side-item">
                    <div class="side-image">
                        <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                            data-src="{{ asset('uploads/posts/'.$bottom_game_news_post->gambar) }}" href="#"
                            title="{{ $bottom_game_news_post->judul }}"></a>

                    </div>
                    <div class="side-item-text">
                        <h4 class="side-title-post">
                            <a href="{{ route('isi_blog', $bottom_game_news_post->slug) }}" rel="bookmark" title="{{ $bottom_game_news_post->judul }}">
                                {{ $bottom_game_news_post->judul }}</a>
                        </h4>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </aside>
    @endforeach
</div>
<div class="footer-widget-wrapper footer-widget-style-4 last">
    <aside id="categories-3" class="widget widget_categories">
        <h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Categories</span></h4>
        <ul>
            @foreach ($categories as $bottom_categories)
            <li class="cat-item cat-item-3"><a href="{{ route('kategori', $bottom_categories->slug) }}">{{ $bottom_categories->name }}<span
                        class="category-item-count">{{ $bottom_categories->post->count() }}</span></a>
            </li>
            @endforeach
        </ul>

    </aside>
</div>
