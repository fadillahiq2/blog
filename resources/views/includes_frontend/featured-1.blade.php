<div class="elementor-container elementor-column-gap-wider">
    <div class="elementor-row">
        @foreach ($features_1 as $category)
        <div class="penci-ercol-50 penci-ercol-order-1 penci-sticky-ct  elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-cc50ad5"
            data-id="cc50ad5" data-element_type="column">
            <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div class="elementor-element elementor-element-3d1ecb6 elementor-widget elementor-widget-penci-featured-cat"
                        data-id="3d1ecb6" data-element_type="widget" data-widget_type="penci-featured-cat.default">
                        <div class="elementor-widget-container">
                            <div id="pencifeatured_cat_71155" class="penci-featured-cat-sc">
                                <section class="home-featured-cat mag-cat-style-1">
                                    <div
                                        class="penci-border-arrow penci-homepage-title penci-magazine-title style-12 pcalign-left">
                                        <h3 class="inner-arrow">
                                            {{ $category->name }} </h3>
                                    </div>
                                    <div class="home-featured-cat-content style-1">
                                        <div class="cat-left">
                                            @foreach ($category->post->sortByDesc('created_at')->take(1) as $mag)
                                            <div class="mag-post-box hentry first-post">
                                                <div class="magcat-thumb">
                                                    <a class="penci-image-holder penci-lazy"
                                                        data-src="{{ asset('uploads/posts/'.$mag->gambar) }}" href="{{ route('isi_blog', $mag->slug) }}"
                                                        title="{{ $mag->judul }}">
                                                    </a>
                                                </div>
                                                <div class="magcat-detail">
                                                    <div class="mag-header">
                                                        <h3 class="magcat-titlte entry-title">
                                                            <a href="{{ route('isi_blog', $mag->slug) }}">{{ $mag->judul }}</a>
                                                        </h3>
                                                        <div class="grid-post-box-meta mag-meta">
                                                            <span class="featc-author author-italic author vcard">by
                                                                <a class="url fn n"
                                                                    href="#">{{
                                                                    $mag->user->name }}</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="mag-excerpt entry-content">
                                                        <p>{!! Str::limit($mag->content, 174, ' ...') !!}</p>
                                                    </div>
                                                    <div class="penci-hide-tagupdated">
                                                        <span class="author-italic author vcard">by
                                                            <a class="url fn n" href="#">{{ $mag->user->name
                                                                }}</a></span>
                                                        <time class="entry-date published"
                                                            datetime="2019-10-15T07:45:22+00:00">{{
                                                            \Carbon\Carbon::parse($mag->created_at)->format('d M Y')
                                                            }}</time>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="cat-right">
                                            @foreach ($category->post->sortBy('created_at')->take(4) as $post)
                                            <div class="mag-post-box hentry">
                                                <div class="magcat-thumb">
                                                    <a class="penci-image-holder penci-lazy small-fix-size"
                                                        data-src="{{ asset('uploads/posts/'.$post->gambar) }}"
                                                        href="{{ route('isi_blog', $post->slug) }}"
                                                        title="Call of Duty: Mobile&#8217;s Microtransactions Analyzed Could Be Worse">
                                                    </a>
                                                </div>
                                                <div class="magcat-detail">
                                                    <h3 class="magcat-titlte entry-title">
                                                        <a
                                                            href="{{ route('isi_blog', $post->slug) }}">{{
                                                            $post->judul }}</a>
                                                    </h3>
                                                    <div class="penci-hide-tagupdated">
                                                        <span class="author-italic author vcard">by
                                                            <a class="url fn n" href="#">{{
                                                                $post->user->name }}</a></span>
                                                        <time class="entry-date published"
                                                            datetime="{{ \Carbon\Carbon::parse($post->created_at)->format('d M Y') }}">{{ \Carbon\Carbon::parse($post->created_at)->format('d M Y') }}</time>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </section>
                            </div><!-- penci-featured-cat-sc -->
                            <style>
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
