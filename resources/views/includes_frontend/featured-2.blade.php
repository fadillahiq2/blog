<div class="elementor-shape elementor-shape-top" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9" />
    </svg>
</div>
<div class="elementor-shape elementor-shape-bottom" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9" />
    </svg>
</div>
<div class="elementor-container elementor-column-gap-wider">
    <div class="elementor-row">
        @foreach ($categories->sortByDesc('created_at')->take(2) as $category)
        <div class="penci-ercol-50 penci-ercol-order-1 penci-sticky-ct  elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ef0a87e"
            data-id="ef0a87e" data-element_type="column">
            <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div class="elementor-element elementor-element-211a9a9 elementor-widget elementor-widget-penci-featured-cat"
                        data-id="211a9a9" data-element_type="widget" data-widget_type="penci-featured-cat.default">
                        <div class="elementor-widget-container">
                            <div id="pencifeatured_cat_42078" class="penci-featured-cat-sc">
                                <section class="home-featured-cat mag-cat-style-7">
                                    <div
                                        class="penci-border-arrow penci-homepage-title penci-magazine-title style-12 pcalign-left">
                                        <h3 class="inner-arrow">
                                            {{ $category->name }} </h3>
                                    </div>

                                    <div class="home-featured-cat-content style-7">

                                        <ul class="penci-grid penci-grid-maglayout penci-fea-cat-style-7">
                                            @foreach ($category->post->take(4) as $kategori)
                                            <li class="grid-style">
                                                <article id="post-102" class="item hentry">
                                                    <div class="thumbnail">
                                                        <a class="penci-image-holder penci-lazy"
                                                            data-src="{{ asset('uploads/posts/'.$kategori->gambar) }}"
                                                            href="{{ route('isi_blog', $kategori->slug) }}" title="{{ $kategori->judul }}">
                                                        </a>

                                                    </div>

                                                    <div class="grid-header-box">

                                                        <h2 class="grid-title entry-title">
                                                            <a href="{{ route('isi_blog', $kategori->slug) }}">{{ $kategori->judul }}</a>
                                                        </h2>

                                                    </div>

                                                    <div class="penci-hide-tagupdated">
                                                        <span class="author-italic author vcard">by
                                                            <a class="url fn n" href="#">{{ $kategori->user->name
                                                                }}</a></span>
                                                        <time class="entry-date published"
                                                            datetime="{{ \Carbon\Carbon::parse($kategori->created_at)->format('d M Y') }}">{{ \Carbon\Carbon::parse($kategori->created_at)->format('d M Y') }}</time>
                                                    </div>
                                                </article>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </section>
                            </div><!-- penci-featured-cat-sc -->
                            <style>
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
