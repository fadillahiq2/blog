<footer id="footer-section" class="penci-footer-social-media penci-lazy footer-social-remove-circle" itemscope
    itemtype="https://schema.org/WPFooter">
    <div class="container container-1400">
        <div class="footer-socials-section">
            <ul class="footer-socials">
                <li><a href="https://www.facebook.com/sky.lord.high" aria-label="Facebook" rel="noreferrer"
                        target="_blank"><i class="penci-faicon fa fa-facebook"></i><span>Facebook</span></a>
                </li>
                <li><a href="https://www.instagram.com/fadillahiq" aria-label="Instagram" rel="noreferrer" target="_blank"><i
                            class="penci-faicon fa fa-instagram"></i><span>Instagram</span></a></li>
                <li><a href="https://www.linkedin.com/in/fadillahiq" aria-label="LinkedIn" rel="noreferrer" target="_blank"><i
                            class="penci-faicon fa fa-linkedin"></i><span>LinkedIn</span></a></li>
            </ul>
        </div>
        <div class="footer-logo-copyright footer-not-logo footer-not-gotop">


            <div id="footer-copyright">
                <p>@2021 - All Right Reserved. Designed by <a rel="nofollow"
                        href="https://1.envato.market/YYJ4P" target="_blank">PenciDesign</a></p>
            </div>
        </div>
        <div class="penci-go-to-top-floating"><i class="penci-faicon fa fa-angle-up"></i></div>
    </div>
</footer>
