<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11" />
<link rel="shortcut icon" href="{{ asset('blog/soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg') }}"
    type="image/x-icon" />
<link rel="apple-touch-icon" sizes="180x180"
    href="{{ asset('blog/soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg') }}">
<link rel="alternate" type="application/rss+xml" title="Soledad &#8211; Game News Two Sidebars RSS Feed"
    href="feed/index.html" />
<link rel="alternate" type="application/atom+xml" title="Soledad &#8211; Game News Two Sidebars Atom Feed"
    href="feed/atom/index.html" />
<link rel="pingback" href="xmlrpc.php" />
<!--[if lt IE 9]>
	<script src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/js/html5.js"></script>
	<![endif]-->
<title>Soledad &#8211; Game News Two Sidebars &#8211; Multi-purpose WordPress Themes site</title>
<meta name='robots' content='max-image-preview:large' />
<meta name="description" content="Soledad WordPress Theme - Responsive Multi-Purpose AMP WordPress Theme" />
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Soledad - Game News Two Sidebars &raquo; Feed"
    href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Soledad - Game News Two Sidebars &raquo; Comments Feed"
    href="comments/feed/index.html" />
<script type="text/javascript">
    window._wpemojiSettings = { "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/", "svgExt": ".svg", "source": { "wpemoji": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-includes\/js\/wp-emoji.js?ver=5.7.2", "twemoji": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-includes\/js\/twemoji.js?ver=5.7.2" } };
    /**
* @output wp-includes/js/wp-emoji-loader.js
*/

    (function (window, document, settings) {
        var src, ready, ii, tests;

        // Create a canvas element for testing native browser support of emoji.
        var canvas = document.createElement('canvas');
        var context = canvas.getContext && canvas.getContext('2d');

        /**
         * Checks if two sets of Emoji characters render the same visually.
         *
         * @since 4.9.0
         *
         * @private
         *
         * @param {number[]} set1 Set of Emoji character codes.
         * @param {number[]} set2 Set of Emoji character codes.
         *
         * @return {boolean} True if the two sets render the same.
         */
        function emojiSetsRenderIdentically(set1, set2) {
            var stringFromCharCode = String.fromCharCode;

            // Cleanup from previous test.
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillText(stringFromCharCode.apply(this, set1), 0, 0);
            var rendered1 = canvas.toDataURL();

            // Cleanup from previous test.
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillText(stringFromCharCode.apply(this, set2), 0, 0);
            var rendered2 = canvas.toDataURL();

            return rendered1 === rendered2;
        }

        /**
         * Detects if the browser supports rendering emoji or flag emoji.
         *
         * Flag emoji are a single glyph made of two characters, so some browsers
         * (notably, Firefox OS X) don't support them.
         *
         * @since 4.2.0
         *
         * @private
         *
         * @param {string} type Whether to test for support of "flag" or "emoji".
         *
         * @return {boolean} True if the browser can render emoji, false if it cannot.
         */
        function browserSupportsEmoji(type) {
            var isIdentical;

            if (!context || !context.fillText) {
                return false;
            }

            /*
             * Chrome on OS X added native emoji rendering in M41. Unfortunately,
             * it doesn't work when the font is bolder than 500 weight. So, we
             * check for bold rendering support to avoid invisible emoji in Chrome.
             */
            context.textBaseline = 'top';
            context.font = '600 32px Arial';

            switch (type) {
                case 'flag':
                    /*
                     * Test for Transgender flag compatibility. This flag is shortlisted for the Emoji 13 spec,
                     * but has landed in Twemoji early, so we can add support for it, too.
                     *
                     * To test for support, we try to render it, and compare the rendering to how it would look if
                     * the browser doesn't render it correctly (white flag emoji + transgender symbol).
                     */
                    isIdentical = emojiSetsRenderIdentically(
                        [0x1F3F3, 0xFE0F, 0x200D, 0x26A7, 0xFE0F],
                        [0x1F3F3, 0xFE0F, 0x200B, 0x26A7, 0xFE0F]
                    );

                    if (isIdentical) {
                        return false;
                    }

                    /*
                     * Test for UN flag compatibility. This is the least supported of the letter locale flags,
                     * so gives us an easy test for full support.
                     *
                     * To test for support, we try to render it, and compare the rendering to how it would look if
                     * the browser doesn't render it correctly ([U] + [N]).
                     */
                    isIdentical = emojiSetsRenderIdentically(
                        [0xD83C, 0xDDFA, 0xD83C, 0xDDF3],
                        [0xD83C, 0xDDFA, 0x200B, 0xD83C, 0xDDF3]
                    );

                    if (isIdentical) {
                        return false;
                    }

                    /*
                     * Test for English flag compatibility. England is a country in the United Kingdom, it
                     * does not have a two letter locale code but rather an five letter sub-division code.
                     *
                     * To test for support, we try to render it, and compare the rendering to how it would look if
                     * the browser doesn't render it correctly (black flag emoji + [G] + [B] + [E] + [N] + [G]).
                     */
                    isIdentical = emojiSetsRenderIdentically(
                        [0xD83C, 0xDFF4, 0xDB40, 0xDC67, 0xDB40, 0xDC62, 0xDB40, 0xDC65, 0xDB40, 0xDC6E, 0xDB40, 0xDC67, 0xDB40, 0xDC7F],
                        [0xD83C, 0xDFF4, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC62, 0x200B, 0xDB40, 0xDC65, 0x200B, 0xDB40, 0xDC6E, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC7F]
                    );

                    return !isIdentical;
                case 'emoji':
                    /*
                     * So easy, even a baby could do it!
                     *
                     *  To test for Emoji 13 support, try to render a new emoji: Man Feeding Baby.
                     *
                     * The Man Feeding Baby emoji is a ZWJ sequence combining 👨 Man, a Zero Width Joiner and 🍼 Baby Bottle.
                     *
                     * 0xD83D, 0xDC68 == Man emoji.
                     * 0x200D == Zero-Width Joiner (ZWJ) that links the two code points for the new emoji or
                     * 0x200B == Zero-Width Space (ZWS) that is rendered for clients not supporting the new emoji.
                     * 0xD83C, 0xDF7C == Baby Bottle.
                     *
                     * When updating this test for future Emoji releases, ensure that individual emoji that make up the
                     * sequence come from older emoji standards.
                     */
                    isIdentical = emojiSetsRenderIdentically(
                        [0xD83D, 0xDC68, 0x200D, 0xD83C, 0xDF7C],
                        [0xD83D, 0xDC68, 0x200B, 0xD83C, 0xDF7C]
                    );

                    return !isIdentical;
            }

            return false;
        }

        /**
         * Adds a script to the head of the document.
         *
         * @ignore
         *
         * @since 4.2.0
         *
         * @param {Object} src The url where the script is located.
         * @return {void}
         */
        function addScript(src) {
            var script = document.createElement('script');

            script.src = src;
            script.defer = script.type = 'text/javascript';
            document.getElementsByTagName('head')[0].appendChild(script);
        }

        tests = Array('flag', 'emoji');

        settings.supports = {
            everything: true,
            everythingExceptFlag: true
        };

        /*
         * Tests the browser support for flag emojis and other emojis, and adjusts the
         * support settings accordingly.
         */
        for (ii = 0; ii < tests.length; ii++) {
            settings.supports[tests[ii]] = browserSupportsEmoji(tests[ii]);

            settings.supports.everything = settings.supports.everything && settings.supports[tests[ii]];

            if ('flag' !== tests[ii]) {
                settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && settings.supports[tests[ii]];
            }
        }

        settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && !settings.supports.flag;

        // Sets DOMReady to false and assigns a ready function to settings.
        settings.DOMReady = false;
        settings.readyCallback = function () {
            settings.DOMReady = true;
        };

        // When the browser can not render everything we need to load a polyfill.
        if (!settings.supports.everything) {
            ready = function () {
                settings.readyCallback();
            };

            /*
             * Cross-browser version of adding a dom ready event.
             */
            if (document.addEventListener) {
                document.addEventListener('DOMContentLoaded', ready, false);
                window.addEventListener('load', ready, false);
            } else {
                window.attachEvent('onload', ready);
                document.attachEvent('onreadystatechange', function () {
                    if ('complete' === document.readyState) {
                        settings.readyCallback();
                    }
                });
            }

            src = settings.source || {};

            if (src.concatemoji) {
                addScript(src.concatemoji);
            } else if (src.wpemoji && src.twemoji) {
                addScript(src.twemoji);
                addScript(src.wpemoji);
            }
        }

    })(window, document, window._wpemojiSettings);
</script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel='stylesheet' id='wp-block-library-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/css/dist/block-library/style9f31.css?ver=5.7.2')}}' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-theme-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/css/dist/block-library/theme9f31.css?ver=5.7.2')}}' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/contact-form-7/includes/css/styles91d5.css?ver=5.4')}}' type='text/css' media='all' />
<link rel='stylesheet' id='penci-oswald-css'
    href='http://fonts.googleapis.com/css?family=Oswald%3A400&amp;display=swap&amp;ver=5.7.2' type='text/css'
    media='all' />
<link rel='stylesheet' id='penci-fonts-css'
    href='http://fonts.googleapis.com/css?family=PT+Serif%3A300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C700%2C700italic%2C800%2C800italic%7CPlayfair+Display+SC%3A300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C700%2C700italic%2C800%2C800italic%7CMontserrat%3A300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C700%2C700italic%2C800%2C800italic%7CRoboto%3A300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C700%2C700italic%2C800%2C800italic%26subset%3Dlatin%2Ccyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Clatin-ext&amp;display=swap&amp;ver=1.0'
    type='text/css' media='all' />
<link rel='stylesheet' id='penci-main-style-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/main5125.css?ver=7.9.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='penci-font-awesomeold-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/css/font-awesome.4.7.0.swap.min1849.css?ver=4.7.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='penci-font-iweather-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/css/weather-icon.swapd5f7.css?ver=2.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='penci_style-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/style5125.css?ver=7.9.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons21f9.css?ver=5.11.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/animations/animations.minaeb9.css?ver=3.1.4')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-legacy-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/css/frontend-legacyaeb9.css?ver=3.1.4')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/css/frontendaeb9.css?ver=3.1.4')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-354-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/elementor/css/post-354b40c.css?ver=1618733595')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/elementor/css/globalb40c.css?ver=1618733595')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-6-css' href='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/elementor/css/post-6b40c.css?ver=1618733595')}}' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'
    href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.7.2'
    type='text/css' media='all' />
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/jquery/jquery9d52.js?ver=3.5.1')}}' id='jquery-core-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/jquery/jquery-migrated617.js?ver=3.3.2')}}' id='jquery-migrate-js'></script>
<link rel="https://api.w.org/" href="wp-json/index.html" />
<link rel="alternate" type="application/json" href="wp-json/wp/v2/pages/6.json" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml"
    href="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 5.7.2" />
<link rel="canonical" href="index.html" />
<link rel='shortlink' href='index.html' />
<link rel="alternate" type="application/json+oembed"
    href="wp-json/oembed/1.0/embed073b.json?url=https%3A%2F%2Fdemosoledad.pencidesign.net%2Fsoledad-game-news-two-sidebars%2F" />
<link rel="alternate" type="text/xml+oembed"
    href="wp-json/oembed/1.0/embedda2e?url=https%3A%2F%2Fdemosoledad.pencidesign.net%2Fsoledad-game-news-two-sidebars%2F&amp;format=xml" />
<style type="text/css">
</style>
<style id="penci-custom-style" type="text/css">
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    .penci-lgpop-title,
    .penci-login-register input[type="submit"],
    h2.penci-heading-video,
    #navigation .menu li a,
    .penci-photo-2-effect figcaption h2,
    .headline-title,
    a.penci-topbar-post-title,
    #sidebar-nav .menu li a,
    .penci-slider .pencislider-container .pencislider-content .pencislider-title,
    .penci-slider .pencislider-container .pencislider-content .pencislider-button,
    #main .bbp-login-form .bbp-submit-wrapper button[type="submit"],
    .author-quote span,
    .penci-more-link a.more-link,
    .penci-post-share-box .dt-share,
    .post-share a .dt-share,
    .author-content h5,
    .post-pagination h5,
    .post-box-title,
    .penci-countdown .countdown-amount,
    .penci-countdown .countdown-period,
    .penci-pagination a,
    .penci-pagination .disable-url,
    ul.footer-socials li a span,
    .penci-button,
    .widget input[type="submit"],
    .penci-user-logged-in .penci-user-action-links a,
    .widget button[type="submit"],
    .penci-sidebar-content .widget-title,
    #respond h3.comment-reply-title span,
    .widget-social.show-text a span,
    .footer-widget-wrapper .widget .widget-title,
    .penci-user-logged-in .penci-user-action-links a,
    .container.penci-breadcrumb span,
    .container.penci-breadcrumb span a,
    .penci-container-inside.penci-breadcrumb span,
    .penci-container-inside.penci-breadcrumb span a,
    .container.penci-breadcrumb span,
    .container.penci-breadcrumb span a,
    .error-404 .go-back-home a,
    .post-entry .penci-portfolio-filter ul li a,
    .penci-portfolio-filter ul li a,
    .portfolio-overlay-content .portfolio-short .portfolio-title a,
    .home-featured-cat-content .magcat-detail h3 a,
    .post-entry blockquote cite,
    .post-entry blockquote .author,
    .tags-share-box.hide-tags.page-share .share-title,
    .widget ul.side-newsfeed li .side-item .side-item-text h4 a,
    .thecomment .comment-text span.author,
    .thecomment .comment-text span.author a,
    .post-comments span.reply a,
    #respond h3,
    #respond label,
    .wpcf7 label,
    #respond #submit,
    div.wpforms-container .wpforms-form.wpforms-form .wpforms-field-label,
    div.wpforms-container .wpforms-form.wpforms-form input[type=submit],
    div.wpforms-container .wpforms-form.wpforms-form button[type=submit],
    div.wpforms-container .wpforms-form.wpforms-form .wpforms-page-button,
    .wpcf7 input[type="submit"],
    .widget_wysija input[type="submit"],
    .archive-box span,
    .archive-box h1,
    .gallery .gallery-caption,
    .contact-form input[type=submit],
    ul.penci-topbar-menu>li a,
    div.penci-topbar-menu>ul>li a,
    .featured-style-29 .penci-featured-slider-button a,
    .pencislider-container .pencislider-content .pencislider-title,
    .pencislider-container .pencislider-content .pencislider-button,
    ul.homepage-featured-boxes .penci-fea-in.boxes-style-3 h4 span span,
    .pencislider-container .pencislider-content .pencislider-button,
    .woocommerce div.product .woocommerce-tabs .panel #respond .comment-reply-title,
    .penci-recipe-index-wrap .penci-index-more-link a,
    .penci-menu-hbg .menu li a,
    #sidebar-nav .menu li a,
    .penci-readmore-btn.penci-btn-make-button a,
    .bos_searchbox_widget_class #flexi_searchbox h1,
    .bos_searchbox_widget_class #flexi_searchbox h2,
    .bos_searchbox_widget_class #flexi_searchbox h3,
    .bos_searchbox_widget_class #flexi_searchbox h4,
    .bos_searchbox_widget_class #flexi_searchbox #b_searchboxInc .b_submitButton_wrapper .b_submitButton:hover,
    .bos_searchbox_widget_class #flexi_searchbox #b_searchboxInc .b_submitButton_wrapper .b_submitButton,
    .penci-featured-cat-seemore.penci-btn-make-button a,
    .penci-menu-hbg-inner .penci-hbg_sitetitle {
        font-family: 'Montserrat', sans-serif;
    }

    .featured-style-29 .penci-featured-slider-button a,
    #bbpress-forums #bbp-search-form .button {
        font-weight: bold;
    }

    #main #bbpress-forums .bbp-login-form fieldset.bbp-form select,
    #main #bbpress-forums .bbp-login-form .bbp-form input[type="password"],
    #main #bbpress-forums .bbp-login-form .bbp-form input[type="text"],
    .penci-login-register input[type="email"],
    .penci-login-register input[type="text"],
    .penci-login-register input[type="password"],
    .penci-login-register input[type="number"],
    body,
    textarea,
    #respond textarea,
    .widget input[type="text"],
    .widget input[type="email"],
    .widget input[type="date"],
    .widget input[type="number"],
    .wpcf7 textarea,
    .mc4wp-form input,
    #respond input,
    div.wpforms-container .wpforms-form.wpforms-form input[type=date],
    div.wpforms-container .wpforms-form.wpforms-form input[type=datetime],
    div.wpforms-container .wpforms-form.wpforms-form input[type=datetime-local],
    div.wpforms-container .wpforms-form.wpforms-form input[type=email],
    div.wpforms-container .wpforms-form.wpforms-form input[type=month],
    div.wpforms-container .wpforms-form.wpforms-form input[type=number],
    div.wpforms-container .wpforms-form.wpforms-form input[type=password],
    div.wpforms-container .wpforms-form.wpforms-form input[type=range],
    div.wpforms-container .wpforms-form.wpforms-form input[type=search],
    div.wpforms-container .wpforms-form.wpforms-form input[type=tel],
    div.wpforms-container .wpforms-form.wpforms-form input[type=text],
    div.wpforms-container .wpforms-form.wpforms-form input[type=time],
    div.wpforms-container .wpforms-form.wpforms-form input[type=url],
    div.wpforms-container .wpforms-form.wpforms-form input[type=week],
    div.wpforms-container .wpforms-form.wpforms-form select,
    div.wpforms-container .wpforms-form.wpforms-form textarea,
    .wpcf7 input,
    form.pc-searchform input.search-input,
    ul.homepage-featured-boxes .penci-fea-in h4,
    .widget.widget_categories ul li span.category-item-count,
    .about-widget .about-me-heading,
    .widget ul.side-newsfeed li .side-item .side-item-text .side-item-meta {
        font-family: 'Roboto', sans-serif;
    }

    p {
        line-height: 1.8;
    }

    #navigation .menu li a,
    .penci-menu-hbg .menu li a,
    #sidebar-nav .menu li a {
        font-family: 'Montserrat', sans-serif;
        font-weight: normal;
    }

    .penci-hide-tagupdated {
        display: none !important;
    }

    body,
    .widget ul li a {
        font-size: 15px;
    }

    .widget ul li,
    .post-entry,
    p,
    .post-entry p {
        font-size: 15px;
        line-height: 1.8;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    .penci-lgpop-title,
    #sidebar-nav .menu li a,
    #navigation .menu li a,
    a.penci-topbar-post-title,
    .penci-slider .pencislider-container .pencislider-content .pencislider-title,
    .penci-slider .pencislider-container .pencislider-content .pencislider-button,
    .headline-title,
    .author-quote span,
    .penci-more-link a.more-link,
    .author-content h5,
    .post-pagination h5,
    .post-box-title,
    .penci-countdown .countdown-amount,
    .penci-countdown .countdown-period,
    .penci-pagination a,
    .penci-pagination .disable-url,
    ul.footer-socials li a span,
    .penci-sidebar-content .widget-title,
    #respond h3.comment-reply-title span,
    .widget-social.show-text a span,
    .footer-widget-wrapper .widget .widget-title,
    .error-404 .go-back-home a,
    .home-featured-cat-content .magcat-detail h3 a,
    .post-entry blockquote cite,
    .pencislider-container .pencislider-content .pencislider-title,
    .pencislider-container .pencislider-content .pencislider-button,
    .post-entry blockquote .author,
    .tags-share-box.hide-tags.page-share .share-title,
    .widget ul.side-newsfeed li .side-item .side-item-text h4 a,
    .thecomment .comment-text span.author,
    .thecomment .comment-text span.author a,
    #respond h3,
    #respond label,
    .wpcf7 label,
    div.wpforms-container .wpforms-form.wpforms-form .wpforms-field-label,
    div.wpforms-container .wpforms-form.wpforms-form input[type=submit],
    div.wpforms-container .wpforms-form.wpforms-form button[type=submit],
    div.wpforms-container .wpforms-form.wpforms-form .wpforms-page-button,
    #respond #submit,
    .wpcf7 input[type="submit"],
    .widget_wysija input[type="submit"],
    .archive-box span,
    .penci-login-register input[type="submit"],
    .archive-box h1,
    .gallery .gallery-caption,
    .widget input[type="submit"],
    .penci-button,
    #main .bbp-login-form .bbp-submit-wrapper button[type="submit"],
    .widget button[type="submit"],
    .contact-form input[type=submit],
    ul.penci-topbar-menu>li a,
    div.penci-topbar-menu>ul>li a,
    .penci-recipe-index-wrap .penci-index-more-link a,
    #bbpress-forums #bbp-search-form .button,
    .penci-menu-hbg .menu li a,
    #sidebar-nav .menu li a,
    .penci-readmore-btn.penci-btn-make-button a,
    .penci-featured-cat-seemore.penci-btn-make-button a,
    .penci-menu-hbg-inner .penci-hbg_sitetitle {
        font-weight: 500;
    }

    .featured-area .penci-image-holder,
    .featured-area .penci-slider4-overlay,
    .featured-area .penci-slide-overlay .overlay-link,
    .featured-style-29 .featured-slider-overlay,
    .penci-slider38-overlay {
        border-radius: ;
        -webkit-border-radius: ;
    }

    .penci-featured-content-right:before {
        border-top-right-radius: ;
        border-bottom-right-radius: ;
    }

    .penci-flat-overlay .penci-slide-overlay .penci-mag-featured-content:before {
        border-bottom-left-radius: ;
        border-bottom-right-radius: ;
    }

    .container-single .post-image {
        border-radius: ;
        -webkit-border-radius: ;
    }

    .penci-mega-thumbnail .penci-image-holder {
        border-radius: ;
        -webkit-border-radius: ;
    }

    #navigation .menu li a,
    .penci-menu-hbg .menu li a,
    #sidebar-nav .menu li a {
        font-weight: 500;
    }

    .penci-menuhbg-toggle:hover .lines-button:after,
    .penci-menuhbg-toggle:hover .penci-lines:before,
    .penci-menuhbg-toggle:hover .penci-lines:after,
    .tags-share-box.tags-share-box-s2 .post-share-plike,
    .penci-video_playlist .penci-playlist-title,
    .pencisc-column-2.penci-video_playlist .penci-video-nav .playlist-panel-item,
    .pencisc-column-1.penci-video_playlist .penci-video-nav .playlist-panel-item,
    .penci-video_playlist .penci-custom-scroll::-webkit-scrollbar-thumb,
    .pencisc-button,
    .post-entry .pencisc-button,
    .penci-dropcap-box,
    .penci-dropcap-circle,
    .penci-login-register input[type="submit"]:hover,
    .penci-ld .penci-ldin:before,
    .penci-ldspinner>div {
        background: #ff4081;
    }

    a,
    .post-entry .penci-portfolio-filter ul li a:hover,
    .penci-portfolio-filter ul li a:hover,
    .penci-portfolio-filter ul li.active a,
    .post-entry .penci-portfolio-filter ul li.active a,
    .penci-countdown .countdown-amount,
    .archive-box h1,
    .post-entry a,
    .container.penci-breadcrumb span a:hover,
    .post-entry blockquote:before,
    .post-entry blockquote cite,
    .post-entry blockquote .author,
    .wpb_text_column blockquote:before,
    .wpb_text_column blockquote cite,
    .wpb_text_column blockquote .author,
    .penci-pagination a:hover,
    ul.penci-topbar-menu>li a:hover,
    div.penci-topbar-menu>ul>li a:hover,
    .penci-recipe-heading a.penci-recipe-print,
    .penci-review-metas .penci-review-btnbuy,
    .main-nav-social a:hover,
    .widget-social .remove-circle a:hover i,
    .penci-recipe-index .cat>a.penci-cat-name,
    #bbpress-forums li.bbp-body ul.forum li.bbp-forum-info a:hover,
    #bbpress-forums li.bbp-body ul.topic li.bbp-topic-title a:hover,
    #bbpress-forums li.bbp-body ul.forum li.bbp-forum-info .bbp-forum-content a,
    #bbpress-forums li.bbp-body ul.topic p.bbp-topic-meta a,
    #bbpress-forums .bbp-breadcrumb a:hover,
    #bbpress-forums .bbp-forum-freshness a:hover,
    #bbpress-forums .bbp-topic-freshness a:hover,
    #buddypress ul.item-list li div.item-title a,
    #buddypress ul.item-list li h4 a,
    #buddypress .activity-header a:first-child,
    #buddypress .comment-meta a:first-child,
    #buddypress .acomment-meta a:first-child,
    div.bbp-template-notice a:hover,
    .penci-menu-hbg .menu li a .indicator:hover,
    .penci-menu-hbg .menu li a:hover,
    #sidebar-nav .menu li a:hover,
    .penci-rlt-popup .rltpopup-meta .rltpopup-title:hover,
    .penci-video_playlist .penci-video-playlist-item .penci-video-title:hover,
    .penci_list_shortcode li:before,
    .penci-dropcap-box-outline,
    .penci-dropcap-circle-outline,
    .penci-dropcap-regular,
    .penci-dropcap-bold {
        color: #ff4081;
    }

    .penci-home-popular-post ul.slick-dots li button:hover,
    .penci-home-popular-post ul.slick-dots li.slick-active button,
    .post-entry blockquote .author span:after,
    .error-image:after,
    .error-404 .go-back-home a:after,
    .penci-header-signup-form,
    .woocommerce span.onsale,
    .woocommerce #respond input#submit:hover,
    .woocommerce a.button:hover,
    .woocommerce button.button:hover,
    .woocommerce input.button:hover,
    .woocommerce nav.woocommerce-pagination ul li span.current,
    .woocommerce div.product .entry-summary div[itemprop="description"]:before,
    .woocommerce div.product .entry-summary div[itemprop="description"] blockquote .author span:after,
    .woocommerce div.product .woocommerce-tabs #tab-description blockquote .author span:after,
    .woocommerce #respond input#submit.alt:hover,
    .woocommerce a.button.alt:hover,
    .woocommerce button.button.alt:hover,
    .woocommerce input.button.alt:hover,
    #top-search.shoping-cart-icon>a>span,
    #penci-demobar .buy-button,
    #penci-demobar .buy-button:hover,
    .penci-recipe-heading a.penci-recipe-print:hover,
    .penci-review-metas .penci-review-btnbuy:hover,
    .penci-review-process span,
    .penci-review-score-total,
    #navigation.menu-style-2 ul.menu ul:before,
    #navigation.menu-style-2 .menu ul ul:before,
    .penci-go-to-top-floating,
    .post-entry.blockquote-style-2 blockquote:before,
    #bbpress-forums #bbp-search-form .button,
    #bbpress-forums #bbp-search-form .button:hover,
    .wrapper-boxed .bbp-pagination-links span.current,
    #bbpress-forums #bbp_reply_submit:hover,
    #bbpress-forums #bbp_topic_submit:hover,
    #main .bbp-login-form .bbp-submit-wrapper button[type="submit"]:hover,
    #buddypress .dir-search input[type=submit],
    #buddypress .groups-members-search input[type=submit],
    #buddypress button:hover,
    #buddypress a.button:hover,
    #buddypress a.button:focus,
    #buddypress input[type=button]:hover,
    #buddypress input[type=reset]:hover,
    #buddypress ul.button-nav li a:hover,
    #buddypress ul.button-nav li.current a,
    #buddypress div.generic-button a:hover,
    #buddypress .comment-reply-link:hover,
    #buddypress input[type=submit]:hover,
    #buddypress div.pagination .pagination-links .current,
    #buddypress div.item-list-tabs ul li.selected a,
    #buddypress div.item-list-tabs ul li.current a,
    #buddypress div.item-list-tabs ul li a:hover,
    #buddypress table.notifications thead tr,
    #buddypress table.notifications-settings thead tr,
    #buddypress table.profile-settings thead tr,
    #buddypress table.profile-fields thead tr,
    #buddypress table.wp-profile-fields thead tr,
    #buddypress table.messages-notices thead tr,
    #buddypress table.forum thead tr,
    #buddypress input[type=submit] {
        background-color: #ff4081;
    }

    .penci-pagination ul.page-numbers li span.current,
    #comments_pagination span {
        color: #fff;
        background: #ff4081;
        border-color: #ff4081;
    }

    .footer-instagram h4.footer-instagram-title>span:before,
    .woocommerce nav.woocommerce-pagination ul li span.current,
    .penci-pagination.penci-ajax-more a.penci-ajax-more-button:hover,
    .penci-recipe-heading a.penci-recipe-print:hover,
    .penci-review-metas .penci-review-btnbuy:hover,
    .home-featured-cat-content.style-14 .magcat-padding:before,
    .wrapper-boxed .bbp-pagination-links span.current,
    #buddypress .dir-search input[type=submit],
    #buddypress .groups-members-search input[type=submit],
    #buddypress button:hover,
    #buddypress a.button:hover,
    #buddypress a.button:focus,
    #buddypress input[type=button]:hover,
    #buddypress input[type=reset]:hover,
    #buddypress ul.button-nav li a:hover,
    #buddypress ul.button-nav li.current a,
    #buddypress div.generic-button a:hover,
    #buddypress .comment-reply-link:hover,
    #buddypress input[type=submit]:hover,
    #buddypress div.pagination .pagination-links .current,
    #buddypress input[type=submit],
    form.pc-searchform.penci-hbg-search-form input.search-input:hover,
    form.pc-searchform.penci-hbg-search-form input.search-input:focus,
    .penci-dropcap-box-outline,
    .penci-dropcap-circle-outline {
        border-color: #ff4081;
    }

    .woocommerce .woocommerce-error,
    .woocommerce .woocommerce-info,
    .woocommerce .woocommerce-message {
        border-top-color: #ff4081;
    }

    .penci-slider ol.penci-control-nav li a.penci-active,
    .penci-slider ol.penci-control-nav li a:hover,
    .penci-related-carousel .owl-dot.active span,
    .penci-owl-carousel-slider .owl-dot.active span {
        border-color: #ff4081;
        background-color: #ff4081;
    }

    .woocommerce .woocommerce-message:before,
    .woocommerce form.checkout table.shop_table .order-total .amount,
    .woocommerce ul.products li.product .price ins,
    .woocommerce ul.products li.product .price,
    .woocommerce div.product p.price ins,
    .woocommerce div.product span.price ins,
    .woocommerce div.product p.price,
    .woocommerce div.product .entry-summary div[itemprop="description"] blockquote:before,
    .woocommerce div.product .woocommerce-tabs #tab-description blockquote:before,
    .woocommerce div.product .entry-summary div[itemprop="description"] blockquote cite,
    .woocommerce div.product .entry-summary div[itemprop="description"] blockquote .author,
    .woocommerce div.product .woocommerce-tabs #tab-description blockquote cite,
    .woocommerce div.product .woocommerce-tabs #tab-description blockquote .author,
    .woocommerce div.product .product_meta>span a:hover,
    .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
    .woocommerce ul.cart_list li .amount,
    .woocommerce ul.product_list_widget li .amount,
    .woocommerce table.shop_table td.product-name a:hover,
    .woocommerce table.shop_table td.product-price span,
    .woocommerce table.shop_table td.product-subtotal span,
    .woocommerce-cart .cart-collaterals .cart_totals table td .amount,
    .woocommerce .woocommerce-info:before,
    .woocommerce div.product span.price,
    .penci-container-inside.penci-breadcrumb span a:hover {
        color: #ff4081;
    }

    .standard-content .penci-more-link.penci-more-link-button a.more-link,
    .penci-readmore-btn.penci-btn-make-button a,
    .penci-featured-cat-seemore.penci-btn-make-button a {
        background-color: #ff4081;
        color: #fff;
    }

    .penci-vernav-toggle:before {
        border-top-color: #ff4081;
        color: #fff;
    }

    .headline-title {
        background-color: #ff4081;
    }

    .headline-title.nticker-style-2:after,
    .headline-title.nticker-style-4:after {
        border-color: #ff4081;
    }

    a.penci-topbar-post-title:hover {
        color: #ff4081;
    }

    ul.penci-topbar-menu>li a:hover,
    div.penci-topbar-menu>ul>li a:hover {
        color: #ff4081;
    }

    .penci-topbar-social a:hover {
        color: #ff4081;
    }

    #navigation .menu li a:hover,
    #navigation .menu li.current-menu-item>a,
    #navigation .menu>li.current_page_item>a,
    #navigation .menu li:hover>a,
    #navigation .menu>li.current-menu-ancestor>a,
    #navigation .menu>li.current-menu-item>a {
        color: #ff4081;
    }

    #navigation ul.menu>li>a:before,
    #navigation .menu>ul>li>a:before {
        background: #ff4081;
    }

    #navigation .penci-megamenu .penci-mega-child-categories a.cat-active,
    #navigation .menu .penci-megamenu .penci-mega-child-categories a:hover,
    #navigation .menu .penci-megamenu .penci-mega-latest-posts .penci-mega-post a:hover {
        color: #ff4081;
    }

    #navigation .penci-megamenu .penci-mega-thumbnail .mega-cat-name {
        background: #ff4081;
    }

    #navigation ul.menu>li>a,
    #navigation .menu>ul>li>a {
        font-size: 13px;
    }

    #navigation .penci-megamenu .post-mega-title a {
        font-size: 13px;
    }

    #navigation .penci-megamenu .post-mega-title a {
        text-transform: none;
        letter-spacing: 0;
    }

    #navigation .menu .sub-menu li a:hover,
    #navigation .menu .sub-menu li.current-menu-item>a,
    #navigation .sub-menu li:hover>a {
        color: #ff4081;
    }

    #navigation.menu-style-2 ul.menu ul:before,
    #navigation.menu-style-2 .menu ul ul:before {
        background-color: #ff4081;
    }

    .penci-header-signup-form {
        padding-top: px;
        padding-bottom: px;
    }

    .penci-header-signup-form {
        background-color: #ff4081;
    }

    .header-social a:hover i,
    .main-nav-social a:hover,
    .penci-menuhbg-toggle:hover .lines-button:after,
    .penci-menuhbg-toggle:hover .penci-lines:before,
    .penci-menuhbg-toggle:hover .penci-lines:after {
        color: #ff4081;
    }

    #sidebar-nav .menu li a:hover,
    .header-social.sidebar-nav-social a:hover i,
    #sidebar-nav .menu li a .indicator:hover,
    #sidebar-nav .menu .sub-menu li a .indicator:hover {
        color: #ff4081;
    }

    #sidebar-nav-logo:before {
        background-color: #ff4081;
    }

    .penci-slide-overlay .overlay-link,
    .penci-slider38-overlay,
    .penci-flat-overlay .penci-slide-overlay .penci-mag-featured-content:before {
        opacity: ;
    }

    .penci-item-mag:hover .penci-slide-overlay .overlay-link,
    .featured-style-38 .item:hover .penci-slider38-overlay,
    .penci-flat-overlay .penci-item-mag:hover .penci-slide-overlay .penci-mag-featured-content:before {
        opacity: ;
    }

    .penci-featured-content .featured-slider-overlay {
        opacity: ;
    }

    .featured-style-29 .featured-slider-overlay {
        opacity: ;
    }

    .penci-standard-cat .cat>a.penci-cat-name {
        color: #ff4081;
    }

    .penci-standard-cat .cat:before,
    .penci-standard-cat .cat:after {
        background-color: #ff4081;
    }

    .standard-content .penci-post-box-meta .penci-post-share-box a:hover,
    .standard-content .penci-post-box-meta .penci-post-share-box a.liked {
        color: #ff4081;
    }

    .header-standard .post-entry a:hover,
    .header-standard .author-post span a:hover,
    .standard-content a,
    .standard-content .post-entry a,
    .standard-post-entry a.more-link:hover,
    .penci-post-box-meta .penci-box-meta a:hover,
    .standard-content .post-entry blockquote:before,
    .post-entry blockquote cite,
    .post-entry blockquote .author,
    .standard-content-special .author-quote span,
    .standard-content-special .format-post-box .post-format-icon i,
    .standard-content-special .format-post-box .dt-special a:hover,
    .standard-content .penci-more-link a.more-link,
    .standard-content .penci-post-box-meta .penci-box-meta a:hover {
        color: #ff4081;
    }

    .standard-content .penci-more-link.penci-more-link-button a.more-link {
        background-color: #ff4081;
        color: #fff;
    }

    .standard-content-special .author-quote span:before,
    .standard-content-special .author-quote span:after,
    .standard-content .post-entry ul li:before,
    .post-entry blockquote .author span:after,
    .header-standard:after {
        background-color: #ff4081;
    }

    .penci-more-link a.more-link:before,
    .penci-more-link a.more-link:after {
        border-color: #ff4081;
    }

    .penci-grid li .item h2 a,
    .penci-masonry .item-masonry h2 a,
    .grid-mixed .mixed-detail h2 a,
    .overlay-header-box .overlay-title a {
        text-transform: none;
    }

    .penci-grid li .item h2 a,
    .penci-masonry .item-masonry h2 a {
        letter-spacing: 0;
    }

    .penci-grid .cat a.penci-cat-name,
    .penci-masonry .cat a.penci-cat-name {
        color: #ff4081;
    }

    .penci-grid .cat a.penci-cat-name:after,
    .penci-masonry .cat a.penci-cat-name:after {
        border-color: #ff4081;
    }

    .penci-post-share-box a.liked,
    .penci-post-share-box a:hover {
        color: #ff4081;
    }

    .overlay-post-box-meta .overlay-share a:hover,
    .overlay-author a:hover,
    .penci-grid .standard-content-special .format-post-box .dt-special a:hover,
    .grid-post-box-meta span a:hover,
    .grid-post-box-meta span a.comment-link:hover,
    .penci-grid .standard-content-special .author-quote span,
    .penci-grid .standard-content-special .format-post-box .post-format-icon i,
    .grid-mixed .penci-post-box-meta .penci-box-meta a:hover {
        color: #ff4081;
    }

    .penci-grid .standard-content-special .author-quote span:before,
    .penci-grid .standard-content-special .author-quote span:after,
    .grid-header-box:after,
    .list-post .header-list-style:after {
        background-color: #ff4081;
    }

    .penci-grid .post-box-meta span:after,
    .penci-masonry .post-box-meta span:after {
        border-color: #ff4081;
    }

    .penci-readmore-btn.penci-btn-make-button a {
        background-color: #ff4081;
        color: #fff;
    }

    .penci-grid li.typography-style .overlay-typography {
        opacity: ;
    }

    .penci-grid li.typography-style:hover .overlay-typography {
        opacity: ;
    }

    .penci-grid li.typography-style .item .main-typography h2 a:hover {
        color: #ff4081;
    }

    .penci-grid li.typography-style .grid-post-box-meta span a:hover {
        color: #ff4081;
    }

    .overlay-header-box .cat>a.penci-cat-name:hover {
        color: #ff4081;
    }

    .penci-sidebar-content.style-7 .penci-border-arrow .inner-arrow:before,
    .penci-sidebar-content.style-9 .penci-border-arrow .inner-arrow:before {
        background-color: #ff4081;
    }

    .penci-video_playlist .penci-video-playlist-item .penci-video-title:hover,
    .widget ul.side-newsfeed li .side-item .side-item-text h4 a:hover,
    .widget a:hover,
    .penci-sidebar-content .widget-social a:hover span,
    .widget-social a:hover span,
    .penci-tweets-widget-content .icon-tweets,
    .penci-tweets-widget-content .tweet-intents a,
    .penci-tweets-widget-content .tweet-intents span:after,
    .widget-social.remove-circle a:hover i,
    #wp-calendar tbody td a:hover,
    .penci-video_playlist .penci-video-playlist-item .penci-video-title:hover {
        color: #ff4081;
    }

    .widget .tagcloud a:hover,
    .widget-social a:hover i,
    .widget input[type="submit"]:hover,
    .penci-user-logged-in .penci-user-action-links a:hover,
    .penci-button:hover,
    .widget button[type="submit"]:hover {
        color: #fff;
        background-color: #ff4081;
        border-color: #ff4081;
    }

    .about-widget .about-me-heading:before {
        border-color: #ff4081;
    }

    .penci-tweets-widget-content .tweet-intents-inner:before,
    .penci-tweets-widget-content .tweet-intents-inner:after,
    .pencisc-column-1.penci-video_playlist .penci-video-nav .playlist-panel-item,
    .penci-video_playlist .penci-custom-scroll::-webkit-scrollbar-thumb,
    .penci-video_playlist .penci-playlist-title {
        background-color: #ff4081;
    }

    .penci-owl-carousel.penci-tweets-slider .owl-dots .owl-dot.active span,
    .penci-owl-carousel.penci-tweets-slider .owl-dots .owl-dot:hover span {
        border-color: #ff4081;
        background-color: #ff4081;
    }

    #footer-copyright * {
        font-style: normal;
    }

    .footer-subscribe .widget .mc4wp-form input[type="submit"]:hover {
        background-color: #ff4081;
    }

    .footer-widget-wrapper .penci-tweets-widget-content .icon-tweets,
    .footer-widget-wrapper .penci-tweets-widget-content .tweet-intents a,
    .footer-widget-wrapper .penci-tweets-widget-content .tweet-intents span:after,
    .footer-widget-wrapper .widget ul.side-newsfeed li .side-item .side-item-text h4 a:hover,
    .footer-widget-wrapper .widget a:hover,
    .footer-widget-wrapper .widget-social a:hover span,
    .footer-widget-wrapper a:hover,
    .footer-widget-wrapper .widget-social.remove-circle a:hover i {
        color: #ff4081;
    }

    .footer-widget-wrapper .widget .tagcloud a:hover,
    .footer-widget-wrapper .widget-social a:hover i,
    .footer-widget-wrapper .mc4wp-form input[type="submit"]:hover,
    .footer-widget-wrapper .widget input[type="submit"]:hover,
    .footer-widget-wrapper .penci-user-logged-in .penci-user-action-links a:hover,
    .footer-widget-wrapper .widget button[type="submit"]:hover {
        color: #fff;
        background-color: #ff4081;
        border-color: #ff4081;
    }

    .footer-widget-wrapper .about-widget .about-me-heading:before {
        border-color: #ff4081;
    }

    .footer-widget-wrapper .penci-tweets-widget-content .tweet-intents-inner:before,
    .footer-widget-wrapper .penci-tweets-widget-content .tweet-intents-inner:after {
        background-color: #ff4081;
    }

    .footer-widget-wrapper .penci-owl-carousel.penci-tweets-slider .owl-dots .owl-dot.active span,
    .footer-widget-wrapper .penci-owl-carousel.penci-tweets-slider .owl-dots .owl-dot:hover span {
        border-color: #ff4081;
        background: #ff4081;
    }

    ul.footer-socials li a:hover i {
        background-color: #ff4081;
        border-color: #ff4081;
    }

    ul.footer-socials li a:hover span {
        color: #ff4081;
    }

    .footer-socials-section,
    .penci-footer-social-moved {
        border-color: #212121;
    }

    #footer-section,
    .penci-footer-social-moved {
        background-color: #111111;
    }

    #footer-section .footer-menu li a:hover {
        color: #ff4081;
    }

    .penci-go-to-top-floating {
        background-color: #ff4081;
    }

    #footer-section a {
        color: #ff4081;
    }

    .comment-content a,
    .container-single .post-entry a,
    .container-single .format-post-box .dt-special a:hover,
    .container-single .author-quote span,
    .container-single .author-post span a:hover,
    .post-entry blockquote:before,
    .post-entry blockquote cite,
    .post-entry blockquote .author,
    .wpb_text_column blockquote:before,
    .wpb_text_column blockquote cite,
    .wpb_text_column blockquote .author,
    .post-pagination a:hover,
    .author-content h5 a:hover,
    .author-content .author-social:hover,
    .item-related h3 a:hover,
    .container-single .format-post-box .post-format-icon i,
    .container.penci-breadcrumb.single-breadcrumb span a:hover,
    .penci_list_shortcode li:before,
    .penci-dropcap-box-outline,
    .penci-dropcap-circle-outline,
    .penci-dropcap-regular,
    .penci-dropcap-bold,
    .header-standard .post-box-meta-single .author-post span a:hover {
        color: #ff4081;
    }

    .container-single .standard-content-special .format-post-box,
    ul.slick-dots li button:hover,
    ul.slick-dots li.slick-active button,
    .penci-dropcap-box-outline,
    .penci-dropcap-circle-outline {
        border-color: #ff4081;
    }

    ul.slick-dots li button:hover,
    ul.slick-dots li.slick-active button,
    #respond h3.comment-reply-title span:before,
    #respond h3.comment-reply-title span:after,
    .post-box-title:before,
    .post-box-title:after,
    .container-single .author-quote span:before,
    .container-single .author-quote span:after,
    .post-entry blockquote .author span:after,
    .post-entry blockquote .author span:before,
    .post-entry ul li:before,
    #respond #submit:hover,
    div.wpforms-container .wpforms-form.wpforms-form input[type=submit]:hover,
    div.wpforms-container .wpforms-form.wpforms-form button[type=submit]:hover,
    div.wpforms-container .wpforms-form.wpforms-form .wpforms-page-button:hover,
    .wpcf7 input[type="submit"]:hover,
    .widget_wysija input[type="submit"]:hover,
    .post-entry.blockquote-style-2 blockquote:before,
    .tags-share-box.tags-share-box-s2 .post-share-plike,
    .penci-dropcap-box,
    .penci-dropcap-circle,
    .penci-ldspinner>div {
        background-color: #ff4081;
    }

    .container-single .post-entry .post-tags a:hover {
        color: #fff;
        border-color: #ff4081;
        background-color: #ff4081;
    }

    .container-single .penci-standard-cat .cat>a.penci-cat-name {
        color: #ff4081;
    }

    .container-single .penci-standard-cat .cat:before,
    .container-single .penci-standard-cat .cat:after {
        background-color: #ff4081;
    }

    .container-single .single-post-title {
        text-transform: none;
        letter-spacing: 1px;
    }

    @media only screen and (min-width: 769px) {
        .container-single .single-post-title {
            font-size: 30px;
        }
    }

    .container-single .single-post-title {
        letter-spacing: 0;
    }

    .list-post .header-list-style:after,
    .grid-header-box:after,
    .penci-overlay-over .overlay-header-box:after,
    .home-featured-cat-content .first-post .magcat-detail .mag-header:after {
        content: none;
    }

    .list-post .header-list-style,
    .grid-header-box,
    .penci-overlay-over .overlay-header-box,
    .home-featured-cat-content .first-post .magcat-detail .mag-header {
        padding-bottom: 0;
    }

    .penci-single-style-6 .single-breadcrumb,
    .penci-single-style-5 .single-breadcrumb,
    .penci-single-style-4 .single-breadcrumb,
    .penci-single-style-3 .single-breadcrumb,
    .penci-single-style-9 .single-breadcrumb,
    .penci-single-style-7 .single-breadcrumb {
        text-align: left;
    }

    .container-single .header-standard,
    .container-single .post-box-meta-single {
        text-align: left;
    }

    .rtl .container-single .header-standard,
    .rtl .container-single .post-box-meta-single {
        text-align: right;
    }

    .container-single .post-pagination h5 {
        text-transform: none;
        letter-spacing: 0;
    }

    #respond h3.comment-reply-title span:before,
    #respond h3.comment-reply-title span:after,
    .post-box-title:before,
    .post-box-title:after {
        content: none;
        display: none;
    }

    .container-single .item-related h3 a {
        text-transform: none;
        letter-spacing: 0;
    }

    .tags-share-box.tags-share-box-2_3 .post-share .count-number-like,
    .tags-share-box.tags-share-box-2_3 .post-share a,
    .container-single .post-share a,
    .page-share .post-share a {
        color: #ffffff;
    }

    .container-single .post-share a:hover,
    .container-single .post-share a.liked,
    .page-share .post-share a:hover {
        color: #ffffff;
    }

    .tags-share-box.tags-share-box-2_3 .post-share .count-number-like,
    .post-share .count-number-like {
        color: #ffffff;
    }

    .post-entry a,
    .container-single .post-entry a {
        color: #ff4081;
    }

    ul.homepage-featured-boxes .penci-fea-in:hover h4 span {
        color: #ff4081;
    }

    .penci-home-popular-post .item-related h3 a:hover {
        color: #ff4081;
    }

    .home-featured-cat-content .mag-photo .mag-overlay-photo {
        opacity: ;
    }

    .home-featured-cat-content .mag-photo:hover .mag-overlay-photo {
        opacity: ;
    }

    .inner-item-portfolio:hover .penci-portfolio-thumbnail a:after {
        opacity: ;
    }

    .penci_recent-posts-sc ul.side-newsfeed li .side-item .side-item-text h4 a,
    .widget ul.side-newsfeed li .side-item .side-item-text h4 a {
        font-size: 14px
    }

    .penci-block-vc .style-7.penci-border-arrow .inner-arrow:before,
    .penci-block-vc.style-9 .penci-border-arrow .inner-arrow:before {
        background-color: #ff4081;
    }

    .wrapper-boxed,
    .wrapper-boxed.enable-boxed {
        background-position: center !important;
        background-size: cover !important;
        background-repeat: repeat !important;
    }

    @media only screen and (min-width: 961px) {}
</style>
<script>
    var penciBlocksArray = [];
    var portfolioDataJs = portfolioDataJs || []; var PENCILOCALCACHE = {};
    (function () {
        "use strict";

        PENCILOCALCACHE = {
            data: {},
            remove: function (ajaxFilterItem) {
                delete PENCILOCALCACHE.data[ajaxFilterItem];
            },
            exist: function (ajaxFilterItem) {
                return PENCILOCALCACHE.data.hasOwnProperty(ajaxFilterItem) && PENCILOCALCACHE.data[ajaxFilterItem] !== null;
            },
            get: function (ajaxFilterItem) {
                return PENCILOCALCACHE.data[ajaxFilterItem];
            },
            set: function (ajaxFilterItem, cachedData) {
                PENCILOCALCACHE.remove(ajaxFilterItem);
                PENCILOCALCACHE.data[ajaxFilterItem] = cachedData;
            }
        };
    }
    )(); function penciBlock() {
        this.atts_json = '';
        this.content = '';
    }</script>
<script type="application/ld+json">{
    "@context": "https:\/\/schema.org\/",
    "@type": "organization",
    "@id": "#organization",
    "logo": {
        "@type": "ImageObject",
        "url": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-content\/uploads\/sites\/5\/2019\/10\/logo.png"
    },
    "url": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/",
    "name": "Soledad - Game News Two Sidebars",
    "description": "Multi-purpose WordPress Themes site"
}</script>
<script type="application/ld+json">{
    "@context": "https:\/\/schema.org\/",
    "@type": "WebSite",
    "name": "Soledad - Game News Two Sidebars",
    "alternateName": "Multi-purpose WordPress Themes site",
    "url": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/search\/{search_term}",
        "query-input": "required name=search_term"
    }
}</script>
<link rel="icon" href="{{ asset('blog/soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg') }}" sizes="32x32" />
<link rel="icon" href="{{ asset('blog/soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg') }}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{ asset('blog/soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg') }}" />
<meta name="msapplication-TileImage" content="https://soledad.pencidesign.net/imgthemeforest/penci-favicon.jpg" />
