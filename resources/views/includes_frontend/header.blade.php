<div class="penci-header-wrap">
    <header id="header" class="header-header-6 has-bottom-line" itemscope="itemscope"
        itemtype="https://schema.org/WPHeader">
        <nav id="navigation" class="header-layout-bottom header-6 menu-style-2" role="navigation" itemscope
            itemtype="https://schema.org/SiteNavigationElement">
            <div class="container container-1400">
                <div class="button-menu-mobile header-6"><i class="penci-faicon fa fa-bars"></i></div>
                <div id="logo">
                    <a href="{{ route('blog') }}">
                        <img class="penci-logo"
                            src="{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/logo.png') }}"
                            alt="Soledad &#8211; Game News Two Sidebars" width="446" height="450" />
                    </a>
                </div>
                <ul id="menu-menu" class="menu">
                    <li id="menu-item-8" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8"><a
                            href="{{ route('blog') }}">Home</a></li>
                    @foreach ($navs as $nav)
                    <li id="menu-item-42"
                        class="menu-item menu-item-type-taxonomy menu-item-object-category penci-mega-menu menu-item-42">
                        <a href="{{ route('kategori', $nav->slug) }}">{{ $nav->name }}</a>
                        <ul class="sub-menu">
                            <li id="menu-item-0" class="menu-item-0">
                                <div class="penci-megamenu">
                                    <div class="penci-content-megamenu">
                                        <div class="penci-mega-latest-posts col-mn-5 mega-row-1">
                                            <div class="penci-mega-row penci-mega-8 row-active">
                                                @foreach ($nav->post->take(5) as $nav_item)
                                                <div class="penci-mega-post">
                                                    <div class="penci-mega-thumbnail">
                                                        <span class="mega-cat-name">
                                                            <a href="{{ route('kategori', $nav_item->slug) }}">{{ $nav_item->category->name }}</a>
                                                        </span>
                                                        <a class="penci-image-holder penci-lazy"
                                                            data-src="{{ asset('uploads/posts/'.$nav_item->gambar) }}"
                                                            href="{{ route('isi_blog', $nav_item->slug) }}"
                                                            title="{{ $nav_item->judul }}">
                                                        </a>
                                                    </div>
                                                    <div class="penci-mega-meta">
                                                        <h3 class="post-mega-title">
                                                            <a href="{{ route('isi_blog', $nav_item->slug) }}"
                                                                title="{{ $nav_item->judul }}">{{ $nav_item->judul
                                                                }}</a>
                                                        </h3>
                                                        <p class="penci-mega-date"><time class="entry-date published"
                                                                datetime="{{ \Carbon\Carbon::parse($nav_item->created_at)->format('d M Y') }}">{{
                                                                \Carbon\Carbon::parse($nav_item->created_at)->format('d
                                                                M Y') }}</time></p>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </li>
                    @endforeach
                </ul>
                <div id="top-search" class="penci-top-search">
                    <a class="search-click"><i class="penci-faicon fa fa-search"></i></a>
                    <div class="show-search">
                        <form action="{{ route('cari') }}" method="GET" class="pc-searchform">
                            @csrf
                            <div>
                                <input type="text" class="search-input" placeholder="Type and hit enter..." name="cari" />
                            </div>
                        </form> <a class="search-click close-search"><i class="penci-faicon fa fa-close"></i></a>
                    </div>
                </div>
                <div class="main-nav-social">
                    <div class="inner-header-social">
                        <a href="https://www.facebook.com/sky.lord.high" aria-label="Facebook" rel="noreferrer"
                            target="_blank"><i class="penci-faicon fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/fadillahiq" aria-label="Instagram" rel="noreferrer" target="_blank"><i
                                class="penci-faicon fa fa-instagram"></i></a>
                        <a href="https://www.linkedin.com/in/fadillahiq" aria-label="LinkedIn" rel="noreferrer" target="_blank"><i
                                class="penci-faicon fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </nav><!-- End Navigation -->
    </header>
    <!-- end #header -->
</div>
