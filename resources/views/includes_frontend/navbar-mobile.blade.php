<a id="close-sidebar-nav" class="header-6"><i class="penci-faicon fa fa-close"></i></a>
<nav id="sidebar-nav" class="header-6" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">

    <div id="sidebar-nav-logo">
        <a href="index.html"><img class="penci-lazy"
                src="{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/images/penci-holder.png') }}"
                data-src="https://net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/uploads/sites/5/2019/10/logo.png"
                alt="Soledad &#8211; Game News Two Sidebars" /></a>
    </div>

    <div class="header-social sidebar-nav-social">
        <div class="inner-header-social">
            <a href="https://www.facebook.com/sky.lord.high/" aria-label="Facebook" rel="noreferrer" target="_blank"><i
                    class="penci-faicon fa fa-facebook"></i></a>
            <a href="https://www.instagram.com/fadillahiq/" aria-label="Instagram" rel="noreferrer" target="_blank"><i
                    class="penci-faicon fa fa-instagram"></i></a>
            <a href="https://www.linkedin.com/in/fadillahiq" aria-label="LinkedIn" rel="noreferrer" target="_blank"><i
                    class="penci-faicon fa fa-linkedin"></i></a>
        </div>
    </div>


    <ul id="menu-menu-1" class="menu">
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8"><a href="index.html">Home</a>
        </li>
        @foreach ($navs as $nav)
        <li class="menu-item menu-item-type-taxonomy menu-item-object-category penci-mega-menu menu-item-42"><a
                href="{{ route('kategori', $nav->slug) }}">{{ $nav->name }}</a>
        </li>
        @endforeach
    </ul>
</nav>
