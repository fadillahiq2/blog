<style>
    .penci-demo-buttons {
        text-align: right;
        position: fixed;
        top: 110px;
        right: 0;
        z-index: 10000;
        display: block;
        width: 80px;
    }

    .penci-demo-buttons a {
        width: 70px;
        height: 40px;
        cursor: pointer;
        opacity: 1;
        background: #fff;
        -webkit-box-shadow: -1px 1.8px 4px 0 rgba(0, 0, 0, .1);
        -moz-box-shadow: -1px 1.8px 4px 0 rgba(0, 0, 0, .1);
        box-shadow: -1px 1.8px 4px 0 rgba(0, 0, 0, .1);
        line-height: 40px;
        font-size: 13px;
        text-align: center;
        font-weight: 700;
        text-transform: uppercase;
        border-top: 1px solid #f5f5f5;
        display: inline-block;
        color: #313131;
        font-family: 'Lato', sans-serif;
        clear: both;
    }

    .penci-demo-buttons a.penci-demo-btn2 {
        margin-top: 10px;
    }
</style>
<script data-cfasync="false"
    src="{{ asset('blog/demosoledad.pencidesign.net/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js') }}"></script>
<script>(function () {
        function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                this.value = "http://" + this.value;
            }
        }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields) {
            for (var j = 0; j < urlFields.length; j++) {
                urlFields[j].addEventListener('blur', maybePrefixUrlField);
            }
        }
    })();</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/vendor/wp-polyfill89b1.js?ver=7.4.4')}}' id='wp-polyfill-js'></script>
<script type='text/javascript' id='wp-polyfill-js-after'>
    ('fetch' in window) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - fetch6e0e.js ? ver = 3.0.0') }}"></scr' + 'ipt>'); (document.contains) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - node - contains2e00.js ? ver = 3.42.0') }}"></scr' + 'ipt>'); (window.DOMRect) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - dom - rect2e00.js ? ver = 3.42.0') }}"></scr' + 'ipt>'); (window.URL && window.URL.prototype && window.URLSearchParams) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - url5aed.js ? ver = 3.6.4') }}"></scr' + 'ipt>'); (window.FormData && window.FormData.prototype.keys) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - formdatae9bd.js ? ver = 3.0.12') }}"></scr' + 'ipt>'); (Element.prototype.matches && Element.prototype.closest) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - element - closest4c56.js ? ver = 2.0.2') }}"></scr' + 'ipt>'); ('objectFit' in document.documentElement.style) || document.write('<script src="{{ asset('blog / net - demosoledad - gavu0xo8w0ybxpt6g.netdna - ssl.com / soledad - game - news - two - sidebars / wp - includes / js / dist / vendor / wp - polyfill - object - fit531b.js ? ver = 2.3.4') }}"></scr' + 'ipt>');
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/hooksf521.js?ver=50e23bed88bcb9e6e14023e9961698c1')}}' id='wp-hooks-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/i18n87d6.js?ver=db9a9a37da262883343e941c3731bc67')}}' id='wp-i18n-js'></script>
<script type='text/javascript' id='wp-i18n-js-after'>
    wp.i18n.setLocaleData({ 'text direction\u0004ltr': ['ltr'] });
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/vendor/lodashf492.js?ver=4.17.19')}}' id='lodash-js'></script>
<script type='text/javascript' id='lodash-js-after'>
    window.lodash = _.noConflict();
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/urlacd8.js?ver=0ac7e0472c46121366e7ce07244be1ac')}}' id='wp-url-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-translations'>
    (function (domain, translations) {
        var localeData = translations.locale_data[domain] || translations.locale_data.messages;
        localeData[""].domain = domain;
        wp.i18n.setLocaleData(localeData, domain);
    })("default", { "locale_data": { "messages": { "": {} } } });
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/dist/api-fetchf3b9.js?ver=a783d1f442d2abefc7d6dbd156a44561')}}' id='wp-api-fetch-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-after'>
    wp.apiFetch.use(wp.apiFetch.createRootURLMiddleware("wp-json/index.html"));
    wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware("f28737b8b7");
    wp.apiFetch.use(wp.apiFetch.nonceMiddleware);
    wp.apiFetch.use(wp.apiFetch.mediaUploadMiddleware);
    wp.apiFetch.nonceEndpoint = "wp-admin/admin-ajaxf809.html?action=rest-nonce";
</script>
<script type='text/javascript' id='contact-form-7-js-extra'>
    /* <![CDATA[ */
    var wpcf7 = { "cached": "1" };
/* ]]> */
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/contact-form-7/includes/js/index91d5.js?ver=5.4')}}' id='contact-form-7-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/penci-review/js/jquery.easypiechart.min5152.js?ver=1.0')}}' id='jquery-penci-piechart-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/penci-review/js/review5152.js?ver=1.0')}}' id='jquery-penci-review-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/js/libs-script.min5125.js?ver=7.9.0')}}' id='penci-libs-js-js'></script>
<script type='text/javascript' id='main-scripts-js-extra'>
    /* <![CDATA[ */
    var ajax_var_more = { "url": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-admin\/admin-ajax.php", "nonce": "ce826dd0ae", "errorPass": "<p class=\"message message-error\">Password does not match the confirm password<\/p>", "login": "Email Address", "password": "Password" };
/* ]]> */
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/js/main5125.js?ver=7.9.0')}}' id='main-scripts-js'></script>
<script type='text/javascript' id='penci_ajax_like_post-js-extra'>
    /* <![CDATA[ */
    var ajax_var = { "url": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-admin\/admin-ajax.php", "nonce": "ce826dd0ae", "errorPass": "<p class=\"message message-error\">Password does not match the confirm password<\/p>", "login": "Email Address", "password": "Password" };
/* ]]> */
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/themes/soledad/js/post-like5125.js?ver=7.9.0')}}' id='penci_ajax_like_post-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/comment-reply9f31.js?ver=5.7.2')}}' id='comment-reply-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/wp-embed9f31.js?ver=5.7.2')}}' id='wp-embed-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/mailchimp-for-wp/assets/js/forms7bcd.js?ver=4.8.3')}}' id='mc4wp-forms-api-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/js/webpack.runtimeaeb9.js?ver=3.1.4')}}' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/js/frontend-modulesaeb9.js?ver=3.1.4')}}' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-includes/js/jquery/ui/core35d0.js?ver=1.12.1')}}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/dialog/dialoga288.js?ver=4.8.1')}}' id='elementor-dialog-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/waypoints/waypoints05da.js?ver=4.0.2')}}' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/share-link/share-linkaeb9.js?ver=3.1.4')}}' id='share-link-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/lib/swiper/swiper48f5.js?ver=5.3.6')}}' id='swiper-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = { "environmentMode": { "edit": false, "wpPreview": false, "isScriptDebug": true, "isImprovedAssetsLoading": false }, "i18n": { "shareOnFacebook": "Share on Facebook", "shareOnTwitter": "Share on Twitter", "pinIt": "Pin it", "download": "Download", "downloadImage": "Download image", "fullscreen": "Fullscreen", "zoom": "Zoom", "share": "Share", "playVideo": "Play Video", "previous": "Previous", "next": "Next", "close": "Close" }, "is_rtl": false, "breakpoints": { "xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600 }, "version": "3.1.4", "is_static": false, "experimentalFeatures": [], "urls": { "assets": "https:\/\/demosoledad.pencidesign.net\/soledad-game-news-two-sidebars\/wp-content\/plugins\/elementor\/assets\/" }, "settings": { "page": [], "editorPreferences": [] }, "kit": { "global_image_lightbox": "yes", "lightbox_enable_counter": "yes", "lightbox_enable_fullscreen": "yes", "lightbox_enable_zoom": "yes", "lightbox_enable_share": "yes", "lightbox_title_src": "title", "lightbox_description_src": "description" }, "post": { "id": 6, "title": "Soledad%20%E2%80%93%20Game%20News%20Two%20Sidebars%20%E2%80%93%20Multi-purpose%20WordPress%20Themes%20site", "excerpt": "", "featuredImage": false } };
</script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/js/frontendaeb9.js?ver=3.1.4')}}' id='elementor-frontend-js'></script>
<script type='text/javascript' src='{{ asset('blog/net-demosoledad-gavu0xo8w0ybxpt6g.netdna-ssl.com/soledad-game-news-two-sidebars/wp-content/plugins/elementor/assets/js/preloaded-elements-handlersaeb9.js?ver=3.1.4')}}' id='preloaded-elements-handlers-js'></script>
