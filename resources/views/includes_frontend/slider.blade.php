<div class="elementor-background-overlay"></div>
<div class="elementor-shape elementor-shape-bottom" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9" />
    </svg>
</div>
<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div class="penci-ercol-100 penci-ercol-order-1 penci-sticky-ct  elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cb7b447"
            data-id="cb7b447" data-element_type="column">
            <div class="theiaStickySidebar elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div class="elementor-element elementor-element-c6a304f elementor-widget elementor-widget-penci-featured-sliders"
                        data-id="c6a304f" data-element_type="widget" data-widget_type="penci-featured-sliders.default">
                        <div class="elementor-widget-container">
                            <div class="penci-block-el featured-area featured-style-24">
                                <div class="penci-owl-carousel penci-owl-featured-area elsl-style-24"
                                    data-style="style-24" data-auto="true" data-autotime="4000" data-speed="800"
                                    data-loop="true">
                                    <div class="item">
                                        <div class="wrapper-item wrapper-item-classess">
                                            @foreach ($slider->take(4) as $slide)
                                           <div class="penci-item-mag penci-item-2 penci-pitem-small">
                                            <a class="penci-image-holder owl-lazy"
                                                data-src="{{ asset('uploads/posts/'.$slide->gambar) }}"
                                                href="{{ route('isi_blog', $slide->slug) }}"
                                                title="{{ $slide->judul }}"></a>
                                            <div class="penci-slide-overlay penci-slider6-overlay">
                                                <a class="overlay-link"
                                                    aria-label="Apple Arcade Adds 5 More Games..."
                                                    href="{{ route('isi_blog', $slide->slug) }}"></a>
                                                <div class="penci-mag-featured-content">
                                                    <div class="feat-text">
                                                        <h3><a title="{{ $slide->judul }}"
                                                                href="{{ route('isi_blog', $slide->slug) }}">{{ $slide->judul }}</a>
                                                        </h3>
                                                        <div class="feat-meta">
                                                            <span class="feat-time"><time
                                                                    class="entry-date published"
                                                                    datetime="2019-10-15T07:42:54+00:00">{{ \Carbon\Carbon::parse($slide->created_at)->isoFormat('D MMM Y') }}</time></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                           @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
