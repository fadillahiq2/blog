<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title', config('app.name'))</title>

  <!-- General CSS Files -->
    @include('includes_backend.style')
    @stack('style')
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
        @include('includes_backend.navbar')
      <div class="main-sidebar">
        @include('includes_backend.sidebar')
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            @yield('header')
          </div>

          <div class="section-body">
              @yield('content')
          </div>
        </section>
      </div>
      @include('includes_backend.footer')
    </div>
  </div>

  <!-- General JS Scripts -->
  @include('includes_backend.script')

  <!-- Page Specific JS File -->
  @stack('script')
</body>
</html>
