<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from demosoledad.pencidesign.net/soledad-game-news-two-sidebars/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 17:11:36 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    @include('includes_frontend.head')
</head>


<body
    class="home page-template page-template-page-fullwidth page-template-page-fullwidth-php page page-id-6 elementor-default elementor-kit-354 elementor-page elementor-page-6">
    <div class="wrapper-boxed header-style-header-6">
        @include('includes_frontend.header')
        <div class="container-single-page container-default-page">
            <div id="main" class="penci-main-single-page-default">
                <div data-elementor-type="wp-page" data-elementor-id="6" class="elementor elementor-6"
                    data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                           @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-footer"></div>
        <div id="widget-area">
            <div class="container container-1400">
                @include('includes_frontend.content-footer')
            </div>
        </div>
        @include('includes_frontend.footer')

    </div><!-- End .wrapper-boxed -->

    @include('includes_frontend.navbar-mobile')

    @include('includes_frontend.script')


</body>

<!-- Mirrored from demosoledad.pencidesign.net/soledad-game-news-two-sidebars/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 17:12:26 GMT -->

</html>
