<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from demosoledad.pencidesign.net/soledad-game-news-two-sidebars/call-of-duty-dev-says-there-will-be-no-loot-boxes-in-modern-warfare/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 17:13:37 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    @include('includes_frontend.head')
</head>

<body
    class="post-template-default single single-post postid-102 single-format-standard penci-body-single-style-6 penci-show-pthumb penci-two-sidebar elementor-default elementor-kit-354">
    <div class="wrapper-boxed header-style-header-6">
        @include('includes_frontend.header')
        <div class="penci-single-wrapper">
            <div class="penci-single-block"">
		    <div class=" penci-single-pheader container container-single penci-single-style-6 penci-single-smore
                container-single-fullwidth hentry penci-header-text-white two-sidebar">

                @yield('image-content')

            </div>
            <div
                class="container container-single penci-single-style-6 penci-single-smore penci_sidebar two-sidebar penci-enable-lightbox">
                @yield('content')

                <div id="sidebar"
                    class="penci-sidebar-right penci-sidebar-content style-12 pcalign-left penci-sticky-sidebar">
                    <div class="theiaStickySidebar">
                        <aside id="penci_social_widget-2" class="widget penci_social_widget">
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">Social Connect(not working)</span>
                            </h3>
                            <div class="widget-social pc_alignleft show-text">
                                <a href="https://www.facebook.com/sky.lord.high" aria-label="Facebook" rel="noreferrer"
                                    target="_blank"><i class="penci-faicon fa fa-facebook"
                                        style="font-size: 14px"></i><span style="font-size: 13px">Facebook</span></a>
                                <a href="https://www.instagram.com/fadillahiq" aria-label="Instagram" rel="noreferrer" target="_blank"><i
                                        class="penci-faicon fa fa-instagram" style="font-size: 14px"></i><span
                                        style="font-size: 13px">Instagram</span></a>
                                <a href="https://www.linkedin.com/in/fadillahiq" aria-label="LinkedIn" rel="noreferrer" target="_blank"><i
                                        class="penci-faicon fa fa-linkedin" style="font-size: 14px"></i><span
                                        style="font-size: 13px">LinkedIn</span></a>
                            </div>

                        </aside>
                        <aside id="search-2" class="widget widget_search">
                            <form method="GET" class="pc-searchform"
                                action="{{ route('cari') }}">
                                <div>
                                    <input type="text" class="search-input" placeholder="Type and hit enter..."
                                        name="cari" />
                                </div>
                            </form>
                        </aside>
                        <aside id="penci_latest_news_widget-5" class="widget penci_latest_news_widget">
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">Recent Posts</span>
                            </h3>
                            <ul id="penci-latestwg-1413" class="side-newsfeed">
                                @foreach ($posts->take(5) as $recent_post)
                                <li class="penci-feed">
                                    <div class="side-item">

                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="{{ asset('uploads/posts/'.$recent_post->gambar) }}"
                                                href="#"
                                                title="{{ $recent_post->judul }}"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#" rel="bookmark"
                                                    title="{{ $recent_post->judul }}">
                                                    {{ $recent_post->judul }}
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </aside>
                        <aside id="mc4wp_form_widget-2" class="widget widget_mc4wp_form_widget">
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">Newsletter (not working)</span></h3>
                            <script data-cfasync="false"
                                src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                            <script>(function () {
                                    window.mc4wp = window.mc4wp || {
                                        listeners: [],
                                        forms: {
                                            on: function (evt, cb) {
                                                window.mc4wp.listeners.push(
                                                    {
                                                        event: evt,
                                                        callback: cb
                                                    }
                                                );
                                            }
                                        }
                                    }
                                })();
                            </script>
                            <!-- Mailchimp for WordPress v4.8.3 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                            <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-299" method="post" data-id="299"
                                data-name="">
                                <div class="mc4wp-form-fields">
                                    <p class="mdes">Subscribe our newsletter for latest news, service & promo. Let's
                                        stay updated!</p>
                                    <p class="mname"><input type="text" name="NAME" placeholder="Name..." /></p>
                                    <p class="memail"><input type="email" id="mc4wp_email" name="EMAIL"
                                            placeholder="Email..." required /></p>
                                    <p class="msubmit"><input type="submit" value="Subscribe" /></p>
                                </div><label style="display: none !important;">Leave this field empty if you're human:
                                    <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1"
                                        autocomplete="off" /></label><input type="hidden" name="_mc4wp_timestamp"
                                    value="1625677926" /><input type="hidden" name="_mc4wp_form_id" value="299" /><input
                                    type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" />
                                <div class="mc4wp-response"></div>
                            </form><!-- / Mailchimp for WordPress Plugin -->
                        </aside>
                        <aside id="categories-2" class="widget widget_categories">
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">Categories</span></h3>
                            <ul>
                                @foreach ($categories as $right_category)
                                <li class="cat-item cat-item-3"><a
                                        href="{{ route('kategori', $right_category->slug) }}">{{ $right_category->name }}<span
                                            class="category-item-count">{{ $right_category->post->count() }}</span></a>
                                </li>
                                @endforeach
                            </ul>

                        </aside>
                    </div>
                </div>
                <div id="sidebar"
                    class="penci-sidebar-left penci-sidebar-content style-12 pcalign-left penci-sticky-sidebar">
                    <div class="theiaStickySidebar">
                        {{-- <aside id="penci_facebook_widget-2" class="widget penci_facebook_widget">
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">On Facebook</span>
                            </h3>
                            <div id="fb-root"></div>
                            <script data-cfasync="false">(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "../../../connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-page" data-href="https://www.facebook.com/ign" data-height="450"
                                data-small-header="false" data-hide-cover="false" data-show-facepile="true"
                                data-show-posts="true" data-adapt-container-width="true">
                                <div class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ign">On
                                        Facebook</a></div>
                            </div>
                        </aside> --}}
                        <aside id="penci_latest_news_widget-6" class="widget penci_latest_news_widget">
                            @foreach ($categories->where('name', 'Game Reviews')->take(6) as $game_reviews)
                            <h3 class="widget-title penci-border-arrow"><span class="inner-arrow">{{ $game_reviews->name }}</span></h3>
                            <ul id="penci-latestwg-5809" class="side-newsfeed penci-feed-2columns penci-2columns-feed">
                                @foreach ($game_reviews->post as $game_reviews_post)
                                <li class="penci-feed">
                                    <div class="side-item">

                                        <div class="side-image">
                                            <a class="penci-image-holder penci-lazy small-fix-size" rel="bookmark"
                                                data-src="{{ asset('uploads/posts/'.$game_reviews_post->gambar) }}"
                                                href="#"
                                                title="{{ $game_reviews_post->judul }}"></a>

                                        </div>
                                        <div class="side-item-text">
                                            <h4 class="side-title-post">
                                                <a href="#"
                                                    rel="bookmark"
                                                    title="{{ $game_reviews_post->judul }}">
                                                    {{ $game_reviews_post->judul }}
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            @endforeach
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clear-footer"></div>
    <div id="widget-area">
        <div class="container container-1400">
            @include('includes_frontend.content-footer')
        </div>
    </div>
    @include('includes_frontend.footer')
    </div><!-- End .wrapper-boxed -->

    @include('includes_frontend.navbar-mobile')

    @include('includes_frontend.script')
</body>

<!-- Mirrored from demosoledad.pencidesign.net/soledad-game-news-two-sidebars/call-of-duty-dev-says-there-will-be-no-loot-boxes-in-modern-warfare/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Jul 2021 17:13:40 GMT -->

</html>
